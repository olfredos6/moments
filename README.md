# Moments 
![Build are successfull](https://img.shields.io/buildkite/3826789cf8890b426057e6fe1c4e683bdf04fa24d498885489/master.svg)
 ![The project is being reviewed before the development resumes](https://img.shields.io/badge/State-review-orange.svg) ![GNU AFFRERO GPL](https://img.shields.io/badge/license-GNU%20AFFERO%20GPL-blue.svg)


**Moments** is a light desktop app built to automatically grab series, from bittorent clients, one episode after another as they get released. 

## How it works
watch [this short video](https://www.facebook.com/MomentsTVSeriesDownloader/videos/1335210446563314/) to get an idea

## Steps

 - Open the app
 - Type in the name of your favorite series in the search bar and click enter
 - The app will search among popular Bittorent torrents clients and display found series with number of seasons 
 - Click on the add button and the series will be added to your list of followed series
 
 From here, the app will be waiting for the next episode to be available online and automatically download it for you and will notify you when the episode is ready to be watched using your favorite installed video player.
 
The app will keep hosting all downloaded episodes, organised by series name and season.

# Developers

 - Drop me a line if you wish to work with me on the app as there are are plenty of rooms for improvements. My work email address is **nehemie@versalife.co.za**
 - If you wish to use this work, please refer to the terms of the LICENSE found [here](./LICENSE.txt)

# Changes to come
The development of this app stopped for a moment due to design decisions from my side and should resume soon with translating the project to a more convenient stack(Node.js app stack)


---------------------------
by BALUKIDI N  � 2016-2018