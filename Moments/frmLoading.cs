﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

using System.IO;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Net;

namespace Moments
{
    public partial class frmLoading : Form
    {
        string version = File.ReadAllText(Directory.GetCurrentDirectory() + @"\Moments.exe.manifest");
        public frmLoading()
        {
            InitializeComponent();
            pnlProgressBar.Width = 1;
            version = version.Substring(version.IndexOf("name"));
            version = version.Substring(version.IndexOf("version") + 9);
            version = version.Substring(0, version.IndexOf("\""));
            MomentsInitialization.appCurrrentVersion = version;
            lblVersion.Text = "v" + version;
        }
        protected override void OnLoad(EventArgs args)
        {
            Application.Idle += new EventHandler(OnLoaded);
        }

        public void OnLoaded(object sender, EventArgs args)
        {
            try
            {
                Application.Idle -= new EventHandler(OnLoaded);

                lblStatus.Text = "Loading application version information";
                progressBarChange(7);

                MomentsInitialization.directoriesCheck();
                progressBarChange(50);

                lblStatus.Text = "Checking for directories and files integrity: ";
                progressBarChange(63);
                

                MomentsInitialization.buildListOfUsersSeries();
                System.Threading.Tasks.Task t = System.Threading.Tasks.Task.Run(() => MomentsInitialization.populateMomentsUsersSeries());

                progressBarChange(71);
                lblStatus.Text = "Loading content...";
                //MomentsInitialization.builListofFeaturedTVSeries();
                progressBarChange(75);

                lblStatus.Text = "Starting Download Manager....";
                DownloadManager.loadManager();
                Thread.Sleep(5);
                progressBarChange(79);
                lblStatus.Text = "Starting...";
                MomentsInitialization.frmHome = new Home();
                MomentsInitialization.getUserHourFromCyroEST();
                MomentsInitialization.thread_directoriesCheck.IsBackground = true;
                MomentsInitialization.thread_directoriesCheck.Start();//MomentsInitialization.directoriesCheck();
                progressBarChange(83);
                MomentsInitialization.thread_userIDComputation.Start();
                MomentsInitialization.thread_userIDComputation.IsBackground = true;
                DownloadManager.thread_checkToBeDownload.IsBackground = true;
                DownloadManager.thread_checkToBeDownload.Start();

                progressBarChange(93);


                lblStatus.Text = "All set...please wait a moment";
                //MomentsInitialization.frmHome.loadFeturedSeries();

                progressBarChange(100);
                MomentsInitialization.frmHome.Show();
                this.Hide();
            }
            catch (Exception ex){
                MessageBox.Show("No internet connection.\n\nPlease make sure you connect to internet and retry to launch the software." + Environment.NewLine + ex.Message + Environment.NewLine + ex.Source, "Network Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }

        public void progressBarChange(int percent)
        {
            pnlProgressBar.Width = (Convert.ToInt16((percent * this.Width) / 100));
        }

        private void frmLoading_Load(object sender, EventArgs e)
        {

        }
    }
}
