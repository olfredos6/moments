﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.IO;

namespace Moments
{
    public class HitList
    {
        public List<Hit> Hits { get; set; }

        public HitList()
        {
            Hits = new List<Hit>();
        }

        public void cleanList()
        {   //remove those that are not videos, those that are not verified
            foreach (Hit hit in Hits.ToList())
            {
                if (!hit.type.Contains("video tv"))
                {
                    Hits.Remove(hit);
                }
            }

           /* foreach (Hit hit in Hits.ToList())
            {
                if (!hit.isVerified)
                {
                    Hits.Remove(hit);
                }
            }*/
        }

        //This method converts the curentHitList object into a list of hit of the same episode 
        public HitList ToSpecificEpisodeHitsList(string episodeSeason, string episodeNumber) {
            HitList specificEpisodeHitsList = new HitList();

            foreach (Hit hit in Hits) {
                if (hit.Episode == Convert.ToInt32(episodeNumber) ) {
                    specificEpisodeHitsList.Hits.Add(hit);
                }
            }

            return specificEpisodeHitsList;
        }


        //The method that will handle the serach and creation of all by returning a HitList. If no HitList returned, then no results
        public static HitList SearchSeries(string seriesName, string differentialWord = "")
        {
            //int pageNumber = 0;
            string sourceTrackerLink = "https://torrentz2.eu/search?f=";
            HitList SeriesWithEpisodesAndSeason = new HitList();
            try
            {

                while (MomentsInitialization.frmHome.numberOfPageScrapped < 10) {
                    
                    //First we do the torrentz search
                    //string searchResultPage = sourceTrackerLink + seriesName;
                    WebRequest request = WebRequest.Create(sourceTrackerLink + seriesName + "&p=" + MomentsInitialization.frmHome.numberOfPageScrapped);

                    request.Credentials = CredentialCache.DefaultCredentials;

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    Stream dataStream = response.GetResponseStream();

                    StreamReader reader = new StreamReader(dataStream);

                    string htmlCode = reader.ReadToEnd();
                    htmlCode = htmlCode.Substring(htmlCode.IndexOf("<div class=results>"));
                    htmlCode = htmlCode.Substring(0, htmlCode.LastIndexOf("</dl>") + 5);

                    string toCrawl = htmlCode.Substring(htmlCode.IndexOf("<dl>"));
                    List<string> hits = new List<string>();
                    int i = 0;

                    //we add each dl tag as an element in the hit List
                    while (toCrawl.Contains("<dl>"))
                    {
                        string newRecord = toCrawl.Substring(toCrawl.IndexOf("<dl>"));
                        newRecord = newRecord.Substring(0, newRecord.IndexOf("</dl>") + 5);
                        hits.Add(newRecord);
                        toCrawl = toCrawl.Substring(toCrawl.IndexOf(hits[i]) + hits[i].Length);
                        i++;
                    }


                    //Here we change first the seeries name
                    seriesName = seriesName.TrimEnd(' ').Replace(" ", ".").ToLower();
                    List<char> tempName = new List<char>();

                    foreach (char letter in seriesName)
                    {
                        tempName.Add(letter);
                    }

                    tempName[0] = char.ToUpper(tempName[0]);
                    for (int x = 0; x < tempName.Count; x++)
                    {
                        try
                        {
                            if (tempName[x - 1] == '.')
                            {
                                tempName[x] = char.ToUpper(tempName[x]);
                            }
                        }
                        catch { }
                    }

                    seriesName = string.Empty;
                    foreach (char letter in tempName)
                    {
                        seriesName += letter;
                    }



                    //Now we remove every hit that does not contain the serie name or the  differential word
                    foreach (string hit in hits.ToList())
                    {
                        if (seriesName.TrimEnd(' ').Contains(" "))
                        {
                            if (!hit.Contains(seriesName.Replace(" ", ".")) || !hit.Contains(seriesName) || !hit.Contains(seriesName.Replace(" ", ".").ToLower()))
                            {
                                hits.Remove(hit);
                            }
                        }
                        else
                        {
                            if (!hit.Contains(seriesName) || !hit.Contains(seriesName.Replace(" ", ".")))
                            {
                                hits.Remove(hit);
                            }
                        }
                    }

                    //We create the HitList list of all the hit which are left

                    foreach (string dlTag in hits)
                    {
                        try
                        {
                            Hit h = Hit.buildHitFromDLHTMLTag(dlTag);
                            //h.seriesName = seriesName.Replace(".", " ");
                            SeriesWithEpisodesAndSeason.Hits.Add(h);
                        }
                        catch{ }
                    }
                    MomentsInitialization.frmHome.numberOfPageScrapped++;
                    MomentsInitialization.frmHome.pnlSearchProgressBar.Width = Convert.ToInt32(42.3 * MomentsInitialization.frmHome.numberOfPageScrapped);
                }

                MomentsInitialization.frmHome.numberOfPageScrapped = 0;
                return SeriesWithEpisodesAndSeason;
            }
            catch {
                MomentsInitialization.frmHome.numberOfPageScrapped = 0;
                return SeriesWithEpisodesAndSeason;

            }
        }

        public void orderByPeers()
        {    //order the list by an ascending number of peers
            Hits = Hits.OrderByDescending(hit => hit.peers).ToList();
        }
        public void orderBySeason()
        {    //order the list by an ascending number of peers
            Hits = Hits.OrderBy(hit => hit.Season).ToList();
        }
        public void orderByEpisode()
        {    //order the list by an ascending number of peers
            Hits = Hits.OrderBy(hit => hit.Episode).ToList();
        }
        public void orderBySizeOfSameEpisodes()
        {    //order the list by an ascending number of peers

        }

        public int getNumberOfSeason(string _seriesName) {
            
            List<int> seasonsFound = new List<int>();

            foreach (Hit h in Hits) {
                if (h.seriesName == _seriesName) {
                    if (!seasonsFound.Contains(h.Season)) {
                        seasonsFound.Add(h.Season);
                    }
                }
            }

            return seasonsFound.Count();
        }

        //compare the name of series and get the 
    }
}
