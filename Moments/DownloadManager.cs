﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Globalization;

using System.Windows.Forms;

using OctoTorrent.Common;
using OctoTorrent.Client;
using OctoTorrent.BEncoding;
using OctoTorrent.Client.Encryption;
using OctoTorrent.Dht;
using OctoTorrent.Dht.Listeners;
using OctoTorrent.SampleClient;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.IO.Compression;
using ServiceStack;

namespace Moments
{
    public class DownloadManager
    {
        static bool shouldWeStartTheEngine = true;
        private static List<Hit> downloadingHits = new List<Hit>();

        static string _dhtNodeFile;
        static string _basePath;
        public static string _downloadsPath;
        static string _fastResumeFile;
        public static string _torrentsPath;
        static ClientEngine _engine;				// The engine used for downloading
        public static List<TorrentManager> _torrents;	// The list where all the torrentManagers will be stored that the engine gives us
        static Top10Listener _listener;

        static List<Download> listOfDownloadControls = new List<Download>(); //List of all download control showing currentu downloading content

        //this variable might be used later if we think that we can later start downloading even if the day has passed
        //string[] daysOfWeeks = new string[7] { "Monday", "Tuesday", "Wednesday", "Thrusday", "Friday", "Saturday", "Sunday" };
        //We create first the columns of the download list
        public static void loadManager()
        {
            /* Generate the paths to the folder we will save .torrent files to and where we download files to */
            _basePath = MomentsInitialization.path_HomeDirectory;						// This is the directory we are currently in
            _torrentsPath = Path.Combine(_basePath, "Torrents");				// This is the directory we will save .torrents to
            _downloadsPath = Path.Combine(_basePath, "Downloads");			// This is the directory we will save downloads to
            _fastResumeFile = Path.Combine(_torrentsPath, "fastresume.data");
            _dhtNodeFile = Path.Combine(_basePath, "DhtNodes");
            _torrents = new List<TorrentManager>();							// This is where we will store the torrentmanagers
            _listener = new Top10Listener(10);




            // We need to cleanup correctly when the user closes the window by using ctrl-c
            // or an unhandled exception happens
            //Console.CancelKeyPress += delegate { Shutdown(); };
            AppDomain.CurrentDomain.ProcessExit += delegate { Shutdown(); };
            AppDomain.CurrentDomain.UnhandledException += delegate (object _sender, UnhandledExceptionEventArgs _e) { /*MomentsInitialization.frmHome.textBox1.Text = string.Format(_e.ExceptionObject.ToString());*/ Application.Exit(); };
            Thread.GetDomain().UnhandledException += delegate (object _sender, UnhandledExceptionEventArgs _e) { /*MomentsInitialization.frmHome.textBox1.Text = string.Format(_e.ExceptionObject.ToString()); */Application.Exit(); };
        }

        public static Thread thread_checkToBeDownload = new Thread(new ThreadStart(() =>
        {
            thread_checkToBeDownload.IsBackground = true;
            // Create the settings which the engine will use
            // downloadsPath - this is the path where we will save all the files to
            // port - this is the port we listen for connections on
            var engineSettings = new EngineSettings(_downloadsPath, port)
            {
                PreferEncryption = false,
                AllowedEncryption = EncryptionTypes.All
            };


            // Create an instance of the engine.
            _engine = new ClientEngine(engineSettings);
            _engine.ChangeListenEndpoint(new IPEndPoint(IPAddress.Any, port));

            try
            {
                fastResume = BEncodedValue.Decode<BEncodedDictionary>(File.ReadAllBytes(_fastResumeFile));
            }
            catch
            {
                fastResume = new BEncodedDictionary();
            }
            //If the time listed near the series name in user_series file has passed the current time, we go look
            //for the file to download it.
            while (true)
            {
                try
                {
                    //For each series, check if the next episode to be downloaded is out and download it 
                    foreach (TV_Series show in MomentsInitialization.listOfUsersSeries.ToList())
                    {
                        //search for the serie                       

                        HitList ListOfHits = new HitList();
                        ListOfHits = HitList.SearchSeries(show.Name);


                        //We clean the list
                        ListOfHits.cleanList();

                        //We get the new list with only the episode that we are looking for
                        ListOfHits = ListOfHits.ToSpecificEpisodeHitsList(show.currentSeason.ToString(), show.nextEpisode.ToString());//if this fails, then the episode was not found, no need to pursue or return
                                                                                                                                      //null
                        if (ListOfHits.Hits.Count == 0)
                        {
                            continue;
                        }
                        //we order by peeers number for fast dwonload speed since
                        ListOfHits.orderByPeers();


                        //Now we download the first guy(we'll look into whta to with the rest of the guy later)
                        Hit hitToDownload = ListOfHits.Hits[0];

                        //From here, we verify if the a hit with the same series name and the episode number does exists. if it does then we don't download 
                        //it and shift to the next user TV series

                        if (downloadingHits.Exists(xHit => xHit.seriesName == hitToDownload.seriesName && xHit.Episode == hitToDownload.Episode)) { }
                        else
                        {
                            string torrentFilePath = Path.Combine(_torrentsPath, hitToDownload.torrentFileName);
                            if (!File.Exists(torrentFilePath))
                            {

                                //----hitToDownload.getTorrentFileURLFromTorrentProject();
                                hitToDownload.buildTorrentFile();

                                try
                                {
                                    // Load the .torrent from the file into a Torrent instance
                                    // You can use this to do preprocessing should you need to
                                    torrent = Torrent.Load(torrentFilePath);
                                    //To Implement witeOutPut(torrent.InfoHash.ToString());

                                    //We add to currently downloading HIt to prevent the system from dowloading the same episode over and over
                                    downloadingHits.Add(hitToDownload);
                                }
                                catch
                                {
                                    //To Implement witeOutPut(string.Format("Couldn't decode {0}: ", file));
                                    //To Implement witeOutPut(e.Message);
                                    //MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK);
                                    continue;
                                }

                                //Here we verify if the moment is not already part ReadyMoment in order to avoid duplicates
                                bool addTorrent = true;
                                foreach (string readyMoment in File.ReadLines(MomentsInitialization.path_readyMomentsFile))
                                {
                                    if (torrent.Name == readyMoment)
                                    {
                                        addTorrent = false;
                                    }
                                }

                                if (addTorrent)
                                {
                                    //TorrentManager manager = new TorrentManager(hitToDownload.getMagnetLink(), _downloadsPath, torrentDefaults, torrentFilePath);
                                    // When any preprocessing has been completed, you create a TorrentManager
                                    // which you then register with the engine.
                                    var manager = new TorrentManager(torrent, _downloadsPath, torrentDefaults);
                                    listOfanagerAlreadyProcessed.Add(manager);

                                    if (fastResume.ContainsKey(torrent.InfoHash.ToHex()))
                                        manager.LoadFastResume(new FastResume((BEncodedDictionary)fastResume[torrent.InfoHash.ToHex()]));
                                    _engine.Register(manager);

                                    // Store the torrent manager in our list so we can access it later
                                    _torrents.Add(manager);
                                    listOfDownloadControls.Add(new Download(manager.Torrent.Name, manager.Progress, manager.State.ToString()));
                                    manager.PeersFound += ManagerPeersFound;

                                    // Every time a piece is hashed, this is fired.
                                    manager.PieceHashed += delegate (object o, PieceHashedEventArgs e)
                                    {
                                    //witeOutPut(string.Format("Piece Hashed: {0} - {1}", e.PieceIndex, e.HashPassed ? "Pass" : "Fail"));
                                    };

                                    // Every time the state changes (Stopped -> Seeding -> Downloading -> Hashing) this is fired
                                    manager.TorrentStateChanged += delegate (object o, TorrentStateChangedEventArgs e)
                                    {
                                    //witeOutPut(string.Format("OldState: {0} NewState: {1}", e.OldState, e.NewState));
                                    };

                                    // Every time the tracker's state changes, this is fired
                                    foreach (var tracker in manager.TrackerManager.SelectMany(tier => tier.GetTrackers()))
                                    {
                                        //tracker.AnnounceComplete += (sender, e) => witeOutPut(string.Format("{0}: {1}", e.Successful, e.Tracker.ToString()));
                                    }
                                    // Start the torrentmanager. The file will then hash (if required) and begin downloading/seeding
                                    manager.Start();
                                    witeOutPut();
                                }
                                else
                                {
                                    //Since the torrent has been downloaded already, we can delete the downloaded .torrent file
                                    File.Delete(torrentFilePath);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Source, ex.Message, MessageBoxButtons.OK);
                }

                //We do not want thye engine dowloader to start while there are no torrent file to proceed so we create thsi variable which 
                //together with 'shouldWeStartTheEngine' should be true in order to start the downloader

                List<string> listOfTorrentFiles = Directory.GetFiles(_torrentsPath).ToList();
                foreach (string filePath in listOfTorrentFiles.ToList())
                {
                    if (!filePath.EndsWith(".torrent")) { listOfTorrentFiles.Remove(filePath); }
                }

                if (shouldWeStartTheEngine && listOfTorrentFiles.Count > 0)
                {

                    donwloader.IsBackground = true;
                    donwloader.Start();
                    shouldWeStartTheEngine = false;
                    MomentsInitialization.frmHome.thread_CurrentlyDownloading.Start();
                    MomentsInitialization.frmHome.thread_CurrentlyDownloading.IsBackground = true;
                }
                else
                {
                    Thread.Sleep(30000);
                }
            }
        }));

        static int port = 6881;
        static Torrent torrent;
        static BEncodedDictionary fastResume;
        static List<TorrentManager> listOfanagerAlreadyProcessed = new List<TorrentManager>();//This contains a list of manager of torrents file downloaded durring runtime. they are created
                                                                                              //directly after the torrent is download and thhis list is used to prevent them from being processed twice by the engine

        //engineSettings.GlobalMaxUploadSpeed = 30 * 1024;
        //engineSettings.GlobalMaxDownloadSpeed = 100 * 1024;
        //engineSettings.MaxReadRate = 1 * 1024 * 1024;


        // Create the default settings which a torrent will have.
        // 4 Upload slots - a good ratio is one slot per 5kB of upload speed
        // 50 open connections - should never really need to be changed
        // Unlimited download speed - valid range from 0 -> int.Max
        // Unlimited upload speed - valid range from 0 -> int.Max
        static TorrentSettings torrentDefaults = new TorrentSettings(4, 150, 0, 0);

        private static void StartEngine()
        {
            // Ask the user what port they want to use for incoming connections
            //Console.Write(Environment.NewLine + "Choose a listen port: ");
            //while (!Int32.TryParse(Console.ReadLine(), out port)) { }=



            byte[] nodes = null;
            try
            {
                nodes = File.ReadAllBytes(_dhtNodeFile);
            }
            catch
            {
                //To Implement
            }

            DhtListener dhtListner = new DhtListener(new IPEndPoint(IPAddress.Any, port));
            DhtEngine dht = new DhtEngine(dhtListner);
            _engine.RegisterDht(dht);
            dhtListner.Start();
            _engine.DhtEngine.Start(nodes);


            // If the SavePath does not exist, we want to create it.
            if (!Directory.Exists(_engine.Settings.SavePath))
                Directory.CreateDirectory(_engine.Settings.SavePath);

            // If the torrentsPath does not exist, we want to create it
            if (!Directory.Exists(_torrentsPath))
                Directory.CreateDirectory(_torrentsPath);





            // For each file in the torrents path that is a .torrent file, load it into the engine.
            foreach (string file in Directory.GetFiles(_torrentsPath))
            {
                if (file.EndsWith(".torrent"))
                {
                    try
                    {
                        // Load the .torrent from the file into a Torrent instance
                        // You can use this to do preprocessing should you need to
                        torrent = Torrent.Load(file);
                        //To Implement witeOutPut(torrent.InfoHash.ToString());
                    }
                    catch
                    {
                        //To Implement witeOutPut(string.Format("Couldn't decode {0}: ", file));
                        //To Implement witeOutPut(e.Message);
                        continue;
                    }
                    // When any preprocessing has been completed, you create a TorrentManager
                    // which you then register with the engine.
                    bool addTorrent = true;

                    bool retry = true;

                    while (retry)
                    {
                        try
                        {
                            foreach (string readyMoment in File.ReadLines(MomentsInitialization.path_readyMomentsFile))
                            {
                                if (torrent.Name == readyMoment)
                                {
                                    addTorrent = false;
                                }
                            }
                            retry = false;
                        }
                        catch
                        {
                            retry = true;
                        }
                    }

                    if (addTorrent)
                    {

                        var manager = new TorrentManager(torrent, _downloadsPath, torrentDefaults);
                        //manager = new TorrentManager(InfoHash.FromHex(hash), downloadsPath, torrentDefaults, downloadsPathForTorrent, new List<List<string>>());
                        //manager = new TorrentManager(OctoTorrent.InfoHash.FromHex)
                        if (fastResume.ContainsKey(torrent.InfoHash.ToHex()))
                            manager.LoadFastResume(new FastResume((BEncodedDictionary)fastResume[torrent.InfoHash.ToHex()]));
                        try
                        {
                            _engine.Register(manager);

                            // Store the torrent manager in our list so we can access it later
                            _torrents.Add(manager);
                            listOfDownloadControls.Add(new Download(manager.Torrent.Name, manager.Progress, manager.State.ToString()));
                            manager.PeersFound += ManagerPeersFound;
                        }
                        catch
                        {
                            //manager aready registered


                        }
                    }
                }
            }

            witeOutPut();

            // If we loaded no torrents, just exist. The user can put files in the torrents directory and start
            // the client again
            if (_torrents.Count == 0)
            {
                //To Implement witeOutPut("No torrents found in the Torrents directory. Exiting...");
                //_engine.Dispose();
                //return;
            }

            // For each torrent manager we loaded and stored in our list, hook into the events
            // in the torrent manager and start the engine.
            foreach (TorrentManager manager in _torrents)
            {
                if (!listOfanagerAlreadyProcessed.Contains(manager))
                {
                    // Every time a piece is hashed, this is fired.
                    manager.PieceHashed += delegate (object o, PieceHashedEventArgs e) {
                        //witeOutPut(string.Format("Piece Hashed: {0} - {1}", e.PieceIndex, e.HashPassed ? "Pass" : "Fail"));
                    };

                    // Every time the state changes (Stopped -> Seeding -> Downloading -> Hashing) this is fired
                    manager.TorrentStateChanged += delegate (object o, TorrentStateChangedEventArgs e) {
                        //witeOutPut(string.Format("OldState: {0} NewState: {1}", e.OldState, e.NewState));
                    };

                    // Every time the tracker's state changes, this is fired
                    foreach (var tracker in manager.TrackerManager.SelectMany(tier => tier.GetTrackers()))
                    {
                        //tracker.AnnounceComplete += (sender, e) => witeOutPut(string.Format("{0}: {1}", e.Successful, e.Tracker.ToString()));
                    }
                    // Start the torrentmanager. The file will then hash (if required) and begin downloading/seeding
                    manager.Start();
                }
            }




            // While the torrents are still running, print out some stats to the screen.
            // Details for all the loaded torrent managers are shown.
            //var i = 0;
            //var running = true;
            var sb = new StringBuilder(1024);
            while (MomentsInitialization.frmHome.pnlReady.Controls.Count == 0) { }
            while (MomentsInitialization.appIsRunning)
            {
                //MessageBox.Show("Starts", "", MessageBoxButtons.OK);
                if (/*(i++) % 10 == 0*/ _torrents.Count != 0)
                {
                    //sb.Remove(0, sb.Length);
                    //running = _torrents.Exists(delegate (TorrentManager m) { return m.State != TorrentState.Stopped; });
                    //string info = string.Empty;
                    //AppendFormat(sb, "Total Download Rate: {0:0.00}kB/sec", _engine.TotalDownloadSpeed / 1024.0);


                    foreach (var manager in _torrents.ToList())
                    {
                        //MessageBox.Show(manager.Torrent.Name,  "",MessageBoxButtons.OK);
                        //AppendSeperator(sb);
                        //AppendFormat(sb, "State:           {0}", manager.State);
                        //AppendFormat(sb, "Name:            {0}", manager.Torrent == null ? "MetaDataMode" : manager.Torrent.Name);
                        //AppendFormat(sb, "Progress:           {0:0.00}", manager.Progress);
                        //AppendFormat(sb, "Download Speed:     {0:0.00} kB/s", manager.Monitor.DownloadSpeed / 1024.0);
                        //AppendFormat(sb, "Upload Speed:       {0:0.00} kB/s", manager.Monitor.UploadSpeed / 1024.0);
                        //AppendFormat(sb, "Total Downloaded:   {0:0.00} MB", manager.Monitor.DataBytesDownloaded / (1024.0 * 1024.0));
                        //AppendFormat(sb, "Total Uploaded:     {0:0.00} MB", manager.Monitor.DataBytesUploaded / (1024.0 * 1024.0));
                        //var tracker = manager.TrackerManager.CurrentTracker;
                        //AppendFormat(sb, "Tracker Status:     {0}", tracker == null ? "<no tracker>" : tracker.State.ToString());
                        //AppendFormat(sb, "Warning Message:    {0}", tracker == null ? "<no tracker>" : tracker.WarningMessage);
                        //AppendFormat(sb, "Failure Message:    {0}", tracker == null ? "<no tracker>" : tracker.FailureMessage);

                        string torrentName = manager.Torrent.Name;
                        string progress = string.Format("{0:0.00}", manager.Progress);


                        string downloadSpeed = string.Format("{0:0.00}kB/sec", manager.Monitor.DownloadSpeed / 1024);

                        //info = torrentName + "  (" + progress + "%" + ") at " + downloadSpeed;
                        //if (manager.PieceManager != null)
                        //    AppendFormat(sb, "Current Requests:   {0}", manager.PieceManager.CurrentRequestCount());

                        /*foreach (var peerId in manager.GetPeers())
                            AppendFormat(sb, "\t{2} - {1:0.00}/{3:0.00}kB/sec - {0}", peerId.Peer.ConnectionUri,
                                                                                      peerId.Monitor.DownloadSpeed / 1024.0,
                                                                                      peerId.AmRequestingPiecesCount,
                                                                                      peerId.Monitor.UploadSpeed / 1024.0);*/

                        //listOfDownloadControls.Add(new Download(torrentName, manager.Progress));
                        Download dn = MomentsInitialization.frmHome.pnlReady.Controls.Find(torrentName, true).FirstOrDefault() as Download;
                        try
                        {
                            dn.changeProgressBarSize(manager.Progress, manager.State.ToString() + " - " + downloadSpeed + " - " + progress + " %");
                        }
                        catch { }

                        /*AppendFormat(sb, "", null);
                        if (manager.Torrent != null)
                            foreach (var file in manager.Torrent.Files)
                                AppendFormat(sb, "{1:0.00}% - {0}", file.Path, file.BitField.PercentComplete);*/
                        //To Implement witeOutPut(info);

                        if (manager.Complete)
                        {
                            //To Implement witeOutPut(string.Format("Download {0} completed", manager.Torrent.Name));
                            //Send to tht thing that are to be watched 
                            manager.Stop();
                            manager.Dispose();
                            removeCompletedDownload(dn, manager.Torrent.TorrentPath);
                            //MessageBox.Show("Removed the control","", MessageBoxButtons.OK);
                            //_engine.Unregister(manager);
                            //MessageBox.Show("Unregistered the manager", "", MessageBoxButtons.OK);
                            _torrents.Remove(manager);
                            //MessageBox.Show("Removed the manager", "", MessageBoxButtons.OK);
                        }
                    }


                }

                Thread.Sleep(5);
            }
        }

        static void ManagerPeersFound(object sender, PeersAddedEventArgs e)
        {
            //To Implement witeOutPut(string.Format("Found {0} new peers and {1} existing peers", e.NewPeers, e.ExistingPeers));//throw new Exception("The method or operation is not implemented.");
        }

        private static void AppendSeperator(StringBuilder sb)
        {
            AppendFormat(sb, "", null);
            AppendFormat(sb, "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -", null);
            AppendFormat(sb, "", null);
        }
        private static void AppendFormat(StringBuilder sb, string str, params object[] formatting)
        {
            if (formatting != null)
                sb.AppendFormat(str, formatting);
            else
                sb.Append(str);
            sb.AppendLine();
        }


        private static void Shutdown()
        {
            var fastResume = new BEncodedDictionary();
            foreach (var torrentManager in _torrents)
            {
                torrentManager.Stop();
                while (torrentManager.State != TorrentState.Stopped && MomentsInitialization.appIsRunning)
                {
                    //MomentsInitialization.frmHome.textBox1.Text =  string.Format("{0} is {1}", torrentManager.Torrent.Name, torrentManager.State);
                    Thread.Sleep(2);
                }

                fastResume.Add(torrentManager.Torrent.InfoHash.ToHex(), torrentManager.SaveFastResume().Encode());
            }

#if !DISABLE_DHT
            File.WriteAllBytes(_dhtNodeFile, _engine.DhtEngine.SaveNodes());
#endif
            File.WriteAllBytes(_fastResumeFile, fastResume.Encode());
            _engine.Dispose();

            foreach (TraceListener lst in Debug.Listeners)
            {
                lst.Flush();
                lst.Close();
            }

            Thread.Sleep(2000);
        }

        /*********************************************************************/
        delegate void witeOutPutCallBack();

        public static void witeOutPut()
        {
            if (MomentsInitialization.frmHome.pnlReady.InvokeRequired)
            {
                witeOutPutCallBack d = new witeOutPutCallBack(witeOutPut);
                MomentsInitialization.frmHome.Invoke(d, new object[] { });
            }
            else
            {

                MomentsInitialization.frmHome.pnlReady.Controls.Clear();
                foreach (Download content in listOfDownloadControls)
                {
                    MomentsInitialization.frmHome.pnlReady.Controls.Add(content);
                }
                //MomentsInitialization.frmHome.pnlReady.Text = sb;
            }

        }

        public static Thread donwloader = new Thread(new ThreadStart(StartEngine));




        delegate void removeCompletedDownloadCallBack(Download aDownload, string torrentPath);

        public static void removeCompletedDownload(Download aDownload, string torrentPath)
        {
            if (MomentsInitialization.frmHome.pnlReady.InvokeRequired)
            {
                removeCompletedDownloadCallBack d = new removeCompletedDownloadCallBack(removeCompletedDownload);
                MomentsInitialization.frmHome.Invoke(d, new object[] { aDownload, torrentPath });
            }
            else
            {
                //Eventually, we delete the .torrent file so we won't need to manage the torrent anymore
                File.Delete(torrentPath);

                //and we write it to the readyMoments file.
                //I have chosen to inlcude those iinfo inside a file rather than moving the whole file to
                // another directory to avoid massive and excessive load on the client system

                File.AppendAllText(MomentsInitialization.path_readyMomentsFile, aDownload.Name + Environment.NewLine);

                //Now we incremment the episode number for next download
                bool isFileUpdated = false;
                while (!isFileUpdated)
                {
                    try
                    {
                        //we copy all the lines into a string array first
                        List<string> lines = (File.ReadAllLines(MomentsInitialization.path_ShowsFollwoed).ToList());
                        for (int i = 0; i < lines.Count(); i++)
                        {

                            string[] parts = lines[i].Split('#');
                            if (aDownload.Name.Contains(parts[0].Replace(" ", ".")))
                            {
                                parts[2] = (Convert.ToInt32(parts[2]) + 1).ToString();

                                lines[i] = string.Empty;
                                lines[i] = string.Join("#", parts);
                            }
                        }
                        File.Delete(MomentsInitialization.path_ShowsFollwoed);
                        File.Create(MomentsInitialization.path_ShowsFollwoed).Close();

                        File.AppendAllLines(MomentsInitialization.path_ShowsFollwoed, lines);
                        MomentsInitialization.buildListOfUsersSeries();
                        isFileUpdated = true;
                    }
                    catch (Exception ex)
                    {
                        //If the file is being used then we keep the buttons visible so the user can retry
                        MessageBox.Show(ex.Message + Environment.NewLine + ex.Source);
                        Thread.Sleep(6000);
                    }
                }
                //And we delete the control
                MomentsInitialization.frmHome.pnlReady.Controls.Remove(aDownload);
            }
        }
    }
}