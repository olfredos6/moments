﻿namespace Moments
{
    partial class contentToWatchCategory
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btShow = new System.Windows.Forms.Button();
            this.pnlEpisodes = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // btShow
            // 
            this.btShow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.btShow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btShow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btShow.FlatAppearance.BorderSize = 0;
            this.btShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btShow.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btShow.ForeColor = System.Drawing.Color.White;
            this.btShow.Location = new System.Drawing.Point(3, 2);
            this.btShow.Name = "btShow";
            this.btShow.Size = new System.Drawing.Size(457, 36);
            this.btShow.TabIndex = 3;
            this.btShow.Text = "[series title]";
            this.btShow.UseVisualStyleBackColor = false;
            this.btShow.Click += new System.EventHandler(this.btShow_Click);
            // 
            // pnlEpisodes
            // 
            this.pnlEpisodes.AutoSize = true;
            this.pnlEpisodes.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.pnlEpisodes.Location = new System.Drawing.Point(4, 39);
            this.pnlEpisodes.Name = "pnlEpisodes";
            this.pnlEpisodes.Size = new System.Drawing.Size(456, 10);
            this.pnlEpisodes.TabIndex = 4;
            this.pnlEpisodes.Visible = false;
            // 
            // contentToWatchCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Gray;
            this.Controls.Add(this.pnlEpisodes);
            this.Controls.Add(this.btShow);
            this.Name = "contentToWatchCategory";
            this.Size = new System.Drawing.Size(463, 53);
            this.Load += new System.EventHandler(this.contentToWatchCategory_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Button btShow;
        private System.Windows.Forms.FlowLayoutPanel pnlEpisodes;
    }
}
