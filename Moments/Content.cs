﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace Moments
{
    public partial class Content : UserControl
    {
        public string imageURL { get; set; }
        public TV_Series show { get; set; }
        public Content(TV_Series _show)
        {
            show = _show;
            InitializeComponent();
            Title.Text = show.Name;
            tbEpisodeNumber.Text = show.nextEpisode.ToString();
            tbSeasonNumber.Text = show.currentSeason.ToString();
        }


        private void btContentSettngs_Click(object sender, EventArgs e)
        {
            MenuStripContentSettings.Show(Cursor.Position);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            switch (button3.Text) {
                case "save":
                    show.update(Convert.ToInt16(tbSeasonNumber.Text), Convert.ToInt16(tbEpisodeNumber.Text));
                    button3.Text = "edit";
                    tbSeasonNumber.Enabled = false;
                    tbEpisodeNumber.Enabled = false;
                    break;
                case "edit":
                    button3.Text = "save";
                    tbSeasonNumber.Enabled = true;
                    tbEpisodeNumber.Enabled = true;
                    break;
            }
        }

        private void btRemoveFromUserMoments_Click(object sender, EventArgs e)
        {
            show.removeFromUserShowList();
            this.Visible = false;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if (panel2.Visible == true) {
                panel2.Visible = false;
            }
            else
            {
                panel2.Visible = true;
            }
        }
    }
}
