﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace Moments
{
    public class TV_Series
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Genre { get; set; }
        public string Year { get; set; }
        public int NumberOfEpisodes { get; set; }
        public int NumberOfEpisodesAlreadyOut { get; set; }
        public string AirDay { get; set; }
        public string AirTime { get; set; }
        public string Cast { get; set; }
        public string URLSource { get; set; }
        public string ImageURL { get; set; }

        //properties used by new model
        public int nextEpisode { get; set; }
        public int currentSeason { get; set; }

        public TV_Series(string name, int curSeason = 1, int nxtEpisode = 1) {
            Name = name;
            currentSeason = curSeason;
            nextEpisode = nxtEpisode;
        }

        public void addToUserShowList() {
            if (!MomentsInitialization.listOfUsersSeries.Exists(show => show.Name == this.Name)) {
                MomentsInitialization.listOfUsersSeries.Add(this);
            }
            updateShowsFile();
        }

        public void removeFromUserShowList() {
            if (MomentsInitialization.listOfUsersSeries.Exists(show => show.Name == this.Name))
            {
                MomentsInitialization.listOfUsersSeries.Remove(this);
            }
            updateShowsFile();
        }

        public void update(int season, int nextEpisode) {
            MomentsInitialization.listOfUsersSeries.Find(x => x.Name == this.Name).currentSeason = season;
            MomentsInitialization.listOfUsersSeries.Find(x => x.Name == this.Name).nextEpisode = nextEpisode;

            updateShowsFile();
        }

        private static void updateShowsFile() {

            File.Delete(MomentsInitialization.path_ShowsFollwoed);
            File.Create(MomentsInitialization.path_ShowsFollwoed).Close();
            foreach (TV_Series show in MomentsInitialization.listOfUsersSeries) {
                File.AppendAllText(MomentsInitialization.path_ShowsFollwoed, show.Name + "#" + show.currentSeason + "#" + show.nextEpisode + "#" + DateTime.Now + Environment.NewLine);
            }
        }

        private bool isUsersMoment()
        {
            try
            {
                if (MomentsInitialization.listOfUsersSeries.Contains(this))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
