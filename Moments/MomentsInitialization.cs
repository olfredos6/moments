﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO;
using System.Net;

using System.Windows.Forms;
using System.Data;

namespace Moments
{
    class MomentsInitialization
    {
        public static Home frmHome;
        public static string user_id = string.Empty;
        public static Account user_Account;
        public static string appCurrrentVersion = string.Empty;
        public static bool appIsRunning = true;
        #region Global Variables

        public static MySqlConnection connectionToServer = new MySqlConnection("Server=natinstance.caiwfdg20kux.us-west-1.rds.amazonaws.com;Database=natdb;UID=master_NAT;PWD=pass4NATInstance");
        public static List<TV_Series> listOfFeaturedTVSeries = new List<TV_Series>();
        public static List<TV_Series> listOfUsersSeries = new List<TV_Series>();
        public static bool refreshPnlHome = false;


        #endregion

        #region Directories and Files

        public static string path_HomeDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Moments";
        public static string path_userInfoFile = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Moments\\user.txt";
        public static string path_readyMomentsFile = Path.Combine(path_HomeDirectory, "ReadyMoment");
        public static string path_userSettingsFile = Path.Combine(path_HomeDirectory, "Settings");
        public static string path_ShowsFollwoed = Path.Combine(path_HomeDirectory, "Shows");

        public static Thread thread_directoriesCheck = new Thread(new ThreadStart(directoriesCheck));

        public static void directoriesCheck()
        {
            if (!Directory.Exists(path_HomeDirectory))
            {
                Directory.CreateDirectory(path_HomeDirectory);
            }
            if (!File.Exists(path_readyMomentsFile))
            {
                File.Create(path_readyMomentsFile).Close();
            }
            if (!File.Exists(path_userSettingsFile))
            {
                File.Create(path_userSettingsFile).Close();

                //Write settings fields
                File.AppendAllText(path_userSettingsFile, "user_id: " + Environment.NewLine);
                File.AppendAllText(path_userSettingsFile, "user_name: " + Environment.NewLine);
                File.AppendAllText(path_userSettingsFile, "user_mailaddress: " + Environment.NewLine);
                File.AppendAllText(path_userSettingsFile, "user_city: " + Environment.NewLine);
                File.AppendAllText(path_userSettingsFile, "user_preferredlanguage: " + Environment.NewLine);
            }
            if (!File.Exists(path_ShowsFollwoed))
            {
                File.Create(path_ShowsFollwoed).Close();
            }
            // If the SavePath does not exist, we want to create it.
            if (!Directory.Exists(Path.Combine(path_HomeDirectory, "Downloads")))
                Directory.CreateDirectory(Path.Combine(path_HomeDirectory, "Downloads"));

            // If the torrentsPath does not exist, we want to create it
            if (!Directory.Exists(Path.Combine(path_HomeDirectory, "Torrents")))
                Directory.CreateDirectory(Path.Combine(path_HomeDirectory, "Torrents"));
        }

        #endregion


        #region Build list of users TV series
        public static void buildListOfUsersSeries()
        {
            int i = 0;
            if (listOfUsersSeries.Count != 0)
            {
                listOfUsersSeries.Clear();
            }
            try
            {
                foreach (string line in File.ReadAllLines(path_ShowsFollwoed))
                {
                    string toWorkOn = line;
                    listOfUsersSeries.Add(new TV_Series(toWorkOn.Substring(0, toWorkOn.IndexOf("#"))));
                    toWorkOn = toWorkOn.Substring(listOfUsersSeries[i].Name.Length + 1);

                    listOfUsersSeries[i].currentSeason = Convert.ToInt16(toWorkOn.Substring(0, toWorkOn.IndexOf("#")));
                    toWorkOn = toWorkOn.Substring(listOfUsersSeries[i].currentSeason.ToString().Length + 1);

                    listOfUsersSeries[i].nextEpisode = Convert.ToInt16(toWorkOn.Substring(0,toWorkOn.IndexOf("#")));

                    i++;
                }
            }
            catch { }
        }

        #endregion

        #region Get User's number of hour from GMT

        public static TimeSpan getUserHourFromCyroEST()
        {
            TimeSpan x = TimeZone.CurrentTimeZone.GetUtcOffset(System.DateTime.Now) + new TimeSpan(4, 0, 0);
            return x;
        }
        //
        #endregion

        #region User ID computations

        public static Thread thread_userIDComputation = new Thread(new ThreadStart(createnewUserID));
        

        public static void createnewUserID() {
            MomentsInitialization.user_Account = new Account();
            foreach (string line in File.ReadAllLines(path_userSettingsFile))
            {
                if (line.Contains("user_id")) {
                    user_id = line.Substring(line.IndexOf(":") + 2);
                }
            }
                string completedID = string.Empty;
            if (user_id == "" || user_id.Count() < 5)
            {

                bool idExist = false;


                do
                {
                    try
                    {
                        //Get a random number to attach to the id
                        Random x = new Random();


                        string culture = System.Globalization.CultureInfo.CurrentCulture.Name;
                        culture = culture.Substring(culture.IndexOf("-") + 1);
                        completedID = culture + "-" + x.Next(0, 999999).ToString();


                        //check now if we have that ID already in the DB
                        
                        if (Is_ID_in_DB(completedID))
                        {
                            idExist = true;
                        }
                        else
                        {
                            idExist = false;
                            saveNewUserID(completedID);
                        }
                    }
                    catch
                    {
                        idExist = true;
                    }
                } while (idExist);
                user_id = completedID;

                MomentsInitialization.user_Account.editSetting("user_id", user_id);
            }
            else {//We verify if it exists in the DB, if not, we register

                //check now if we have that ID already in the DB
                try
                {
                    if (!Is_ID_in_DB(user_id))
                    {
                        saveNewUserID(user_id);
                    }
                }
                catch {
                }
            }
            displayUserID(user_id);
            frmHome.btUserAccount.Invoke(new Action(() => frmHome.btUserAccount.Visible = true));

            
        }


        delegate void displayUserIDCallBack(string id);
        static void displayUserID(string id) {
            if (frmHome.lblUserID.InvokeRequired) {
                displayUserIDCallBack d = new displayUserIDCallBack(displayUserID);
                frmHome.Invoke(d, new object[] { id });
            }
            else {
                frmHome.lblUserID.Text = "User ID: " + id;
            }
        }

        public static void saveNewUserID(string id) {
            MySqlConnection connectToUsersTable = new MySqlConnection("Server=natinstance.caiwfdg20kux.us-west-1.rds.amazonaws.com;Database=natdb;UID=master_NAT;PWD=pass4NATInstance");
            string insert = "insert into MomentUser values(@user_id, @user_appVersion, default, default, default, default, default)";
            MySqlCommand cmdInsert = new MySqlCommand(insert, connectToUsersTable);
            cmdInsert.Parameters.AddWithValue("@user_id", id);
            cmdInsert.Parameters.AddWithValue("@user_appVersion", appCurrrentVersion);

            connectToUsersTable.Open();
            cmdInsert.ExecuteNonQuery();
            connectToUsersTable.Close();
        }

        public static bool Is_ID_in_DB(string id) {
            string qrySelID = "SELECT COUNT(*) FROM MomentUser WHERE user_id = @user_id";
            MySqlConnection connectToUsersTable = new MySqlConnection("Server=natinstance.caiwfdg20kux.us-west-1.rds.amazonaws.com;Database=natdb;UID=master_NAT;PWD=pass4NATInstance");

            MySqlCommand cmdCount = new MySqlCommand(qrySelID, connectToUsersTable);
            cmdCount.Parameters.AddWithValue("@user_id", user_id);


            connectToUsersTable.Open();
            int exist = Convert.ToInt16(cmdCount.ExecuteScalar());
            connectToUsersTable.Close();

            if (exist == 1) {
                return true;
            }
            else {
                return false;
            }
        }
        #endregion


        #region Thread that add a show to the list of show followed by Moments users
        public static void addToGeneralListofShows() {
            foreach (TV_Series show in listOfUsersSeries)
            {
                //verify if exists in the db, else, we add it
                string qrySel = "select count(*) from PopularShow where showName=@showName";
                MySqlConnection conToPopularShow = connectionToServer;

                MySqlCommand cmdSel = new MySqlCommand(qrySel, conToPopularShow);
                cmdSel.Parameters.AddWithValue("@showName", show.Name);

                if (conToPopularShow.State != ConnectionState.Open)
                {
                    conToPopularShow.Open();
                }
                else
                {
                    conToPopularShow.Close();
                    conToPopularShow.Open();
                }
                int x = Convert.ToInt16(cmdSel.ExecuteScalar());
                conToPopularShow.Close();

                if (x == 0)
                {
                    //Nnot yet saved, we save it
                    MySqlConnection conToPopularShow2 = connectionToServer;
                    qrySel = "insert into PopularShow values(default, @showName, @showLatestSeason, @showLatestEpisode)";

                    MySqlCommand cmdSel2 = new MySqlCommand(qrySel, conToPopularShow2);
                    cmdSel2.Parameters.AddWithValue("@showName", show.Name);
                    cmdSel2.Parameters.AddWithValue("@showLatestSeason", show.currentSeason);
                    cmdSel2.Parameters.AddWithValue("@showLatestEpisode", show.nextEpisode);


                    conToPopularShow2.Open();
                    cmdSel2.ExecuteNonQuery();

                    conToPopularShow2.Close();
                }
                else
                {
                    //we update the values
                }
            }
        }

        #endregion
        

        #region fetch popular shows(it also an update..loool)
        public static void populateMomentsUsersSeries() {
            try
            {
                string qrySel = "select showName as NAME, showLatestSeason as 'LAST SEASON', showLatestEpisode as 'LAST EPISODE'  from PopularShow";
                MySqlConnection conFecthPopularShow = connectionToServer;

                MySqlCommand cmd = new MySqlCommand(qrySel, conFecthPopularShow);

                conFecthPopularShow.Open();

                DataTable dataTable = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(dataTable);

                frmHome.gridShows.Invoke((MethodInvoker)(() => frmHome.gridShows.DataSource = dataTable));
                frmHome.gridShows.Invoke((MethodInvoker)(() => frmHome.gridShows.ReadOnly = true));
                frmHome.gridShows.Invoke((MethodInvoker)(() => frmHome.gridShows.SelectionMode = DataGridViewSelectionMode.FullRowSelect));

                conFecthPopularShow.Close();
            }
            catch //(Exception ex)
            {
                //MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK);
            }

            Search.addItemsToSuggestionList();
        }
        #endregion

    }
}
