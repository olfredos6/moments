﻿namespace Moments
{
    partial class SearchedSeries
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btShow = new System.Windows.Forms.Button();
            this.pnlConfig = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbEpisodeNumber = new System.Windows.Forms.TextBox();
            this.tbSeasonNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.pnlConfig.SuspendLayout();
            this.SuspendLayout();
            // 
            // btShow
            // 
            this.btShow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.btShow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btShow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btShow.FlatAppearance.BorderSize = 0;
            this.btShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btShow.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btShow.ForeColor = System.Drawing.Color.White;
            this.btShow.Location = new System.Drawing.Point(3, 2);
            this.btShow.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btShow.Name = "btShow";
            this.btShow.Size = new System.Drawing.Size(528, 41);
            this.btShow.TabIndex = 4;
            this.btShow.Text = "[series title]";
            this.btShow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btShow.UseVisualStyleBackColor = false;
            this.btShow.Click += new System.EventHandler(this.btShow_Click);
            // 
            // pnlConfig
            // 
            this.pnlConfig.BackColor = System.Drawing.Color.DarkGray;
            this.pnlConfig.Controls.Add(this.button2);
            this.pnlConfig.Controls.Add(this.button1);
            this.pnlConfig.Controls.Add(this.label2);
            this.pnlConfig.Controls.Add(this.label1);
            this.pnlConfig.Controls.Add(this.tbEpisodeNumber);
            this.pnlConfig.Controls.Add(this.tbSeasonNumber);
            this.pnlConfig.Controls.Add(this.label4);
            this.pnlConfig.Location = new System.Drawing.Point(3, 43);
            this.pnlConfig.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlConfig.Name = "pnlConfig";
            this.pnlConfig.Size = new System.Drawing.Size(528, 88);
            this.pnlConfig.TabIndex = 10;
            this.pnlConfig.Visible = false;
            this.pnlConfig.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlConfig_Paint_1);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(333, 31);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(69, 22);
            this.button2.TabIndex = 13;
            this.button2.Text = "close";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(258, 31);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(69, 22);
            this.button1.TabIndex = 12;
            this.button1.Text = "save";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.label2.Location = new System.Drawing.Point(169, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Episode";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.label1.Location = new System.Drawing.Point(86, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Season";
            // 
            // tbEpisodeNumber
            // 
            this.tbEpisodeNumber.BackColor = System.Drawing.Color.White;
            this.tbEpisodeNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEpisodeNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.tbEpisodeNumber.Location = new System.Drawing.Point(221, 31);
            this.tbEpisodeNumber.MaxLength = 2;
            this.tbEpisodeNumber.Name = "tbEpisodeNumber";
            this.tbEpisodeNumber.Size = new System.Drawing.Size(31, 22);
            this.tbEpisodeNumber.TabIndex = 9;
            this.tbEpisodeNumber.TextChanged += new System.EventHandler(this.tbEpisodeNumber_TextChanged);
            // 
            // tbSeasonNumber
            // 
            this.tbSeasonNumber.BackColor = System.Drawing.Color.White;
            this.tbSeasonNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSeasonNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.tbSeasonNumber.Location = new System.Drawing.Point(135, 31);
            this.tbSeasonNumber.MaxLength = 2;
            this.tbSeasonNumber.Name = "tbSeasonNumber";
            this.tbSeasonNumber.Size = new System.Drawing.Size(30, 22);
            this.tbSeasonNumber.TabIndex = 8;
            this.tbSeasonNumber.TextChanged += new System.EventHandler(this.tbSeasonNumber_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(1, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(502, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Please enter from which season and episode number you want to start following thi" +
    "s serie";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.DarkGray;
            this.label7.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.label7.Location = new System.Drawing.Point(4, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "List of empisodes name";
            this.label7.Visible = false;
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.Color.DarkGray;
            this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listBox1.ForeColor = System.Drawing.Color.White;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 15;
            this.listBox1.Location = new System.Drawing.Point(3, 124);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(528, 77);
            this.listBox1.TabIndex = 13;
            this.listBox1.Visible = false;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // SearchedSeries
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Gray;
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btShow);
            this.Controls.Add(this.pnlConfig);
            this.Font = new System.Drawing.Font("Comic Sans MS", 8F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "SearchedSeries";
            this.Size = new System.Drawing.Size(537, 207);
            this.Load += new System.EventHandler(this.SearchedSeries_Load);
            this.pnlConfig.ResumeLayout(false);
            this.pnlConfig.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button btShow;
        private System.Windows.Forms.Panel pnlConfig;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbEpisodeNumber;
        private System.Windows.Forms.TextBox tbSeasonNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox listBox1;
    }
}
