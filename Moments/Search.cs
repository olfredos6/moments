﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Moments
{
    public class Search
    {
        public static void addItemsToSuggestionList()
        {
            string lineSeries = string.Empty;

            #region Object(Toolbox objects used) initialization
            //frmHome.gridShows.Invoke((MethodInvoker)(() => frmHome.gridShows.DataSource = dataTable));
            MomentsInitialization.frmHome.tbSearch.Invoke((MethodInvoker)(() => MomentsInitialization.frmHome.tbSearch.AutoCompleteMode = AutoCompleteMode.Suggest));
            MomentsInitialization.frmHome.tbSearch.Invoke((MethodInvoker)(() => MomentsInitialization.frmHome.tbSearch.AutoCompleteSource = AutoCompleteSource.CustomSource));
            AutoCompleteStringCollection DataCollection = new AutoCompleteStringCollection();
            #endregion
            foreach (DataGridViewRow row in MomentsInitialization.frmHome.gridShows.Rows) {
                DataCollection.Add(row.Cells[0].Value.ToString());
                row.DefaultCellStyle.ForeColor = System.Drawing.Color.White;
            }

            MomentsInitialization.frmHome.gridShows.Invoke((MethodInvoker)(() => MomentsInitialization.frmHome.gridShows.DefaultCellStyle.BackColor = System.Drawing.Color.Gray));
            MomentsInitialization.frmHome.gridShows.Invoke((MethodInvoker)(() => MomentsInitialization.frmHome.gridShows.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.LightGray));
            MomentsInitialization.frmHome.tbSearch.Invoke((MethodInvoker)(() => MomentsInitialization.frmHome.tbSearch.AutoCompleteCustomSource = DataCollection));
        }

        public static TV_Series search(string TVSeries_Name) {
            //Here we only get the TV object and return it
            return MomentsInitialization.listOfFeaturedTVSeries.Find(item => item.Name == TVSeries_Name);
        }
    }
}
