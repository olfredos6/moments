﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Moments
{
    public class panelPendingActions
    {
        #region List of Ready Moments to watch
        //We build a list of all ready momens that can be played
        //file only first
        public static List<string> listOfReadyToWatchMoments = new List<string>(); //paths to thse videos
        public static List<contentToWatchCategory> listOfCategories = new List<contentToWatchCategory>(); //Contains contentCategory controls
        

        public static void buildListOfreadyToWatchMoments() {
            listOfReadyToWatchMoments.Clear();
            listOfCategories.Clear();
            foreach (string pathToContent in File.ReadLines(MomentsInitialization.path_readyMomentsFile)) {
                listOfReadyToWatchMoments.Add(pathToContent);
            }
        }

        #endregion

        #region Display controls of Mmoments that can be wathced
        //Since we are trying to dislay the grouped in categories(by series name), we are going to need 
        //the list of users series. When we take an element in the listOfReadyToWatchMoments, we verify
        //in which category it is and so on on....

        public static void buildCategories() {
            foreach (string episode in listOfReadyToWatchMoments) {
                foreach (TV_Series tvSeries in MomentsInitialization.listOfUsersSeries) {
                    if (episode.Replace(".", " ").Contains(tvSeries.Name)) {
                        //We check if the category already exists and we create it if not
                        if (!listOfCategories.Exists(x => x.Name == tvSeries.Name))
                        {
                            listOfCategories.Add(new contentToWatchCategory(tvSeries.Name));
                        }


                        listOfCategories[listOfCategories.FindIndex(x => x.Name == tvSeries.Name)].addEpisode( new ReadyMoment(episode));
                    }
                }
            }
        }
        #endregion

    }
}
