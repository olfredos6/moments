﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace Moments
{
    class Account
    {
        public string acc_userID { get; set; }
        public string userMailAddress { get; set; }
        public string userName { get; set; }
        public string userCity { get; set; }
        public string userLanguage { get; set; }
        Dictionary<string, string> lines = new Dictionary<string, string>();

        // This contructor fetches all the data contained in the "settings" file and assigns them to each variable property
        public Account(){
            foreach (string line in File.ReadLines(MomentsInitialization.path_userSettingsFile)) {
                if (line.Contains("user_id")) { acc_userID = line.Substring(line.IndexOf(": ") + 2); lines.Add("user_id", acc_userID); }
                if (line.Contains("user_name")) { userName = line.Substring(line.IndexOf(": ") + 2); lines.Add("user_name", userName); }
                if (line.Contains("user_mailaddress")) { userMailAddress = line.Substring(line.IndexOf(": ") + 2); lines.Add("user_mailaddress", userMailAddress); }
                if (line.Contains("user_city")) { userCity = line.Substring(line.IndexOf(": ") + 2); lines.Add("user_city", userCity); }
                if (line.Contains("user_preferredlanguage")) { userLanguage = line.Substring(line.IndexOf(": ") + 2); lines.Add("user_preferredlanguage", userLanguage); }
            }
        }

        public void editSetting(string setting, string newValue) {
            lines[setting] = newValue;

            File.Delete(MomentsInitialization.path_userSettingsFile);
            File.Create(MomentsInitialization.path_userSettingsFile).Close();

            foreach (string key in lines.Keys) {
                File.AppendAllText(MomentsInitialization.path_userSettingsFile, key + ": " + lines[key] + Environment.NewLine);
            }
        }

    }
}
