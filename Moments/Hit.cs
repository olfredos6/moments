﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace Moments
{
    public class Hit
    {
        public string link { get; set; }
        public string type { get; set; }
        public bool isVerified { get; set; }
        public string age { get; set; }
        public string torrentSize { get; set; }
        public double peers { get; set; }

        public int Season { get; set; }
        public int Episode { get; set; }
        public string seriesName { get; set; }
        public string torrentFileName { get; set; }


        public Hit(string _link, string name, string torrentType, string verifiedString, string _age, string size, double _peers)
        {
            //Remove any website's name  in the form www.*.
            Regex rgx = new Regex(@"(w{3})\.(.*)\.(...)");
            name = rgx.Replace(name, "").TrimStart('.', '-', '_');



            link = /*"https://torrentz2.eu/" + */ _link;
            torrentFileName = name;
            type = torrentType;

            if (verifiedString == "✓")
            {
                isVerified = true;
            }
            else
            {
                isVerified = false;
            }

            age = _age;
            torrentSize = size;
            peers = _peers;

            //Now we compute the episode and the episode's season
            string seasonAndEpisode = torrentFileName.Substring(torrentFileName.LastIndexOf(".S") + 2);
            seasonAndEpisode = seasonAndEpisode.Substring(0, seasonAndEpisode.IndexOf("."));

            //Let's first extract the series name
            seriesName = torrentFileName.Substring(0, torrentFileName.IndexOf(seasonAndEpisode) - 2).Trim();

            //== here we check if the names contains anything encapsulated inot []
            if (seriesName.Contains("["))
            {
                //delete it and evryting in it
                seriesName = seriesName.Substring(seriesName.IndexOf("]") + 1).Trim(); //deletes everything that comes before and it

            }
            if (seriesName.Contains("-"))
            {
                //delete it and evryting in it
                seriesName = seriesName.Substring(seriesName.IndexOf("-") + 1).Trim(); //deletes everything that comes before and it

            }
            seriesName = seriesName.Replace("'", "");



            seriesName = seriesName.Replace(".", " ");
            while (seriesName[0] == ' ' || seriesName[seriesName.Length - 1] == ' ')
            {
                seriesName = seriesName.TrimStart();
                seriesName = seriesName.TrimEnd();
            }
            Season = Convert.ToInt16(seasonAndEpisode.Substring(0, seasonAndEpisode.IndexOf("E")));
            Episode = Convert.ToInt16(seasonAndEpisode.Substring(3));

            torrentFileName = buildTorrentFileName();
        }

        public static Hit buildHitFromDLHTMLTag(string encapsulatedDLString)
        {
            //Takes in a string dl tag and work on it to extract all the needed data
            string link = encapsulatedDLString.Substring(encapsulatedDLString.IndexOf("href=/") + 6);
            link = link.Substring(0, link.IndexOf(">"));

            //we remove the part we just worked on(To repeat after next iteration)
            encapsulatedDLString = encapsulatedDLString.Substring(encapsulatedDLString.IndexOf(link) + link.Length + 1);

            //Now we work on the file name then remove the party we've just worked on
            string torrentName = encapsulatedDLString.Substring(0, encapsulatedDLString.IndexOf("<"));
            encapsulatedDLString = encapsulatedDLString.Substring(encapsulatedDLString.IndexOf(torrentName) + torrentName.Length + 4);

            //Now we work on the type of the torrent file
            string type = encapsulatedDLString.Substring(0, encapsulatedDLString.IndexOf("<"));
            encapsulatedDLString = encapsulatedDLString.Substring(encapsulatedDLString.IndexOf(type) + type.Length);

            //now the isVerified string
            string isVerified = encapsulatedDLString.Substring(encapsulatedDLString.IndexOf("<span>") + 6, 1);
            encapsulatedDLString = encapsulatedDLString.Substring(encapsulatedDLString.IndexOf("="));

            //now we work on file age
            string age = encapsulatedDLString.Substring(encapsulatedDLString.IndexOf(">") + 1, encapsulatedDLString.IndexOf("/") - 1);
            age = age.Substring(0, age.IndexOf("<"));
            encapsulatedDLString = encapsulatedDLString.Substring(encapsulatedDLString.IndexOf(age) + age.Length + 13);

            //Now we work on the size
            string size = encapsulatedDLString.Substring(0, encapsulatedDLString.IndexOf("<"));
            encapsulatedDLString = encapsulatedDLString.Substring(encapsulatedDLString.IndexOf(size) + size.Length + 13);

            //Now the number of peers or said seeders(the value wiill have to be converted later)
            string seeders = (encapsulatedDLString.Substring(0, encapsulatedDLString.IndexOf("<")));
            encapsulatedDLString = encapsulatedDLString.Substring(encapsulatedDLString.IndexOf(size) + size.Length + 13);

            //Now we pass the values to create a new Hit object
            return new Hit(link, torrentName, type, isVerified, age, size, Convert.ToDouble(seeders));
        }

        public void getTorrentFileURLFromTorrentProject()
        {
            //this website has all the letter in the url lik capitalized
            string torrentDownloadLink = @"http://torrentproject.se/torrent/" + this.link.ToUpper() + ".torrent";

            //We download it baba
            WebClient webClient = new WebClient();
            webClient.DownloadFile(torrentDownloadLink, Path.Combine(DownloadManager._torrentsPath, this.link + ".torrent"));
        }

        public string getMagnetLink()
        {

            string torrentDownloadLink = @"http://torrentproject.se/" + this.link;

            WebRequest request = WebRequest.Create(torrentDownloadLink);

            request.Credentials = CredentialCache.DefaultCredentials;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream dataStream = response.GetResponseStream();

            StreamReader reader = new StreamReader(dataStream);

            string htmlCode = reader.ReadToEnd();

            string hashCode = htmlCode.Substring(htmlCode.IndexOf("magnet:?"));

            hashCode = hashCode.Substring(0, hashCode.IndexOf("'")).Replace("amp;", "");

            OctoTorrent.MagnetLink link = new OctoTorrent.MagnetLink(hashCode);


            //TorrentManager manager = new TorrentManager(link, engine.Settings.SavePath, new TorrentSettings(), torrentFilePath);

            return hashCode;
        }

        public string getHashCodeString()
        {

            string torrentDownloadLink = @"http://torrentproject.se/" + this.link;

            WebRequest request = WebRequest.Create(torrentDownloadLink);

            request.Credentials = CredentialCache.DefaultCredentials;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream dataStream = response.GetResponseStream();

            StreamReader reader = new StreamReader(dataStream);

            string htmlCode = reader.ReadToEnd();

            string hashCode = htmlCode.Substring(htmlCode.IndexOf("btih:") + 5);

            hashCode = hashCode.Substring(0, hashCode.IndexOf("&")).Replace("amp;", "");



            return hashCode;
        }
        /// <summary>
        /// This method builds the torrent file name
        /// </summary>
        /// <returns></returns>
        private string buildTorrentFileName()
        {
            string fileName = string.Empty;

            fileName = this.seriesName;

            if (this.Season.ToString().Length < 2)
            {
                fileName += "S0" + this.Season;
            }
            else
            {
                fileName += "S" + this.Season;
            }

            if (this.Episode.ToString().Length < 2)
            {
                fileName += "E0" + this.Episode;
            }
            else
            {
                fileName += "E0" + this.Episode;
            }

            return string.Format(fileName + ".torrent").Replace(" ", "");
        }

        /// <summary>
        /// This methods build a torrent file from the magnet link got from the source of hit.
        /// </summary>
        public void buildTorrentFile()
        {
            string magnetLink = this.getMagnetLink();
            string converterEXEPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "converter.exe");

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = converterEXEPath;
            string test = converterEXEPath + "-m \"" + magnetLink + "\" -o " + Path.Combine(DownloadManager._torrentsPath, this.torrentFileName);
            string torrentPath = Path.Combine(DownloadManager._torrentsPath, this.torrentFileName);
            startInfo.Arguments = "-m \"" + magnetLink + "\" -o " + "\"" + torrentPath + "\"";

            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            var converterProcess = Process.Start(startInfo);

            converterProcess.WaitForExit();
        }
    }
}
