﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;



namespace Moments
{
    public partial class ReadyMoment : UserControl
    {

        public ReadyMoment(string title)
        {
            InitializeComponent();
            lblTitle.Text = title;
            this.Name = title;
        }

        private void ReadyMoment_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string contentPath = Path.Combine(DownloadManager._downloadsPath, this.Name);
            //First check if it's a directory or a file
            if (File.Exists(contentPath)) {
                System.Diagnostics.Process.Start(contentPath);
            }
            else if(Directory.Exists(contentPath)){
                System.Diagnostics.Process.Start("explorer.exe", DownloadManager._downloadsPath);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string pathToOpen = Path.Combine(DownloadManager._downloadsPath, this.Name);
            string argument = string.Empty;


            if (File.Exists(pathToOpen))
            {
                argument = "/select, \"" + pathToOpen + "\"";
                System.Diagnostics.Process.Start("explorer.exe", argument);
            }
            else if (Directory.Exists(pathToOpen))
            {
                argument = "/select, \"" + pathToOpen + "\"";
            }
            
        }

        private void btHome_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Are you sure you want to delete " + this.Name + "?", "Confirmation", MessageBoxButtons.YesNo);
            if (r == DialogResult.Yes) {
                try
                {
                    File.Delete(Path.Combine(DownloadManager._downloadsPath, this.Name));

                    List<string> readyMoments = new List<string>();
                    foreach (string line in File.ReadLines(MomentsInitialization.path_readyMomentsFile))
                    {
                        readyMoments.Add(line);
                    }

                    readyMoments.Remove(readyMoments[readyMoments.FindIndex(x => x == this.lblTitle.Text)]);

                    File.Delete(MomentsInitialization.path_readyMomentsFile);
                    File.WriteAllLines(MomentsInitialization.path_readyMomentsFile, readyMoments);
                    panelPendingActions.listOfCategories.Clear();
                    panelPendingActions.listOfReadyToWatchMoments.Clear();
                }
                catch { }
                MomentsInitialization.frmHome.pnlPending.Controls.Clear();
                MomentsInitialization.frmHome.loadPendingMoments();
            }
        }

    }
}
