﻿namespace Moments
{
    partial class Content
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Content));
            this.panel1 = new System.Windows.Forms.Panel();
            this.Title = new System.Windows.Forms.Label();
            this.MenuStripContentSettings = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.downloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.streamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbSeasonNumber = new System.Windows.Forms.TextBox();
            this.tbEpisodeNumber = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btRemoveFromUserMoments = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.MenuStripContentSettings.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkGray;
            this.panel1.Controls.Add(this.btRemoveFromUserMoments);
            this.panel1.Controls.Add(this.Title);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(417, 27);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Location = new System.Drawing.Point(8, 4);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(36, 17);
            this.Title.TabIndex = 0;
            this.Title.Text = "Title";
            // 
            // MenuStripContentSettings
            // 
            this.MenuStripContentSettings.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.downloadToolStripMenuItem,
            this.streamToolStripMenuItem});
            this.MenuStripContentSettings.Name = "MenuStripContentSettings";
            this.MenuStripContentSettings.Size = new System.Drawing.Size(129, 48);
            this.MenuStripContentSettings.Text = "On Aired";
            // 
            // downloadToolStripMenuItem
            // 
            this.downloadToolStripMenuItem.Name = "downloadToolStripMenuItem";
            this.downloadToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.downloadToolStripMenuItem.Text = "Download";
            // 
            // streamToolStripMenuItem
            // 
            this.streamToolStripMenuItem.Name = "streamToolStripMenuItem";
            this.streamToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.streamToolStripMenuItem.Text = "Stream";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(347, 5);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(46, 22);
            this.button3.TabIndex = 13;
            this.button3.Text = "edit";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.label1.Location = new System.Drawing.Point(99, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Current season";
            // 
            // tbSeasonNumber
            // 
            this.tbSeasonNumber.BackColor = System.Drawing.Color.White;
            this.tbSeasonNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSeasonNumber.Enabled = false;
            this.tbSeasonNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.tbSeasonNumber.Location = new System.Drawing.Point(193, 4);
            this.tbSeasonNumber.MaxLength = 2;
            this.tbSeasonNumber.Name = "tbSeasonNumber";
            this.tbSeasonNumber.Size = new System.Drawing.Size(30, 24);
            this.tbSeasonNumber.TabIndex = 8;
            // 
            // tbEpisodeNumber
            // 
            this.tbEpisodeNumber.BackColor = System.Drawing.Color.White;
            this.tbEpisodeNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEpisodeNumber.Enabled = false;
            this.tbEpisodeNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.tbEpisodeNumber.Location = new System.Drawing.Point(311, 4);
            this.tbEpisodeNumber.MaxLength = 2;
            this.tbEpisodeNumber.Name = "tbEpisodeNumber";
            this.tbEpisodeNumber.Size = new System.Drawing.Size(30, 24);
            this.tbEpisodeNumber.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.label2.Location = new System.Drawing.Point(227, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Next episode";
            // 
            // btRemoveFromUserMoments
            // 
            this.btRemoveFromUserMoments.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btRemoveFromUserMoments.BackgroundImage")));
            this.btRemoveFromUserMoments.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btRemoveFromUserMoments.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btRemoveFromUserMoments.FlatAppearance.BorderSize = 0;
            this.btRemoveFromUserMoments.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.btRemoveFromUserMoments.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btRemoveFromUserMoments.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.btRemoveFromUserMoments.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.btRemoveFromUserMoments.Location = new System.Drawing.Point(388, 3);
            this.btRemoveFromUserMoments.Name = "btRemoveFromUserMoments";
            this.btRemoveFromUserMoments.Size = new System.Drawing.Size(26, 26);
            this.btRemoveFromUserMoments.TabIndex = 10;
            this.btRemoveFromUserMoments.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btRemoveFromUserMoments.UseVisualStyleBackColor = true;
            this.btRemoveFromUserMoments.Click += new System.EventHandler(this.btRemoveFromUserMoments_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.tbSeasonNumber);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.tbEpisodeNumber);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(13, 33);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(399, 33);
            this.panel2.TabIndex = 14;
            this.panel2.Visible = false;
            // 
            // Content
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.ForeColor = System.Drawing.Color.White;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Content";
            this.Size = new System.Drawing.Size(417, 71);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.MenuStripContentSettings.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Label Title;
        private System.Windows.Forms.ContextMenuStrip MenuStripContentSettings;
        private System.Windows.Forms.ToolStripMenuItem downloadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem streamToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbEpisodeNumber;
        private System.Windows.Forms.TextBox tbSeasonNumber;
        public System.Windows.Forms.Button button3;
        public System.Windows.Forms.Button btRemoveFromUserMoments;
        private System.Windows.Forms.Panel panel2;
    }
}
