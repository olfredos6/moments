﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

namespace Moments
{
    public partial class SearchedSeries : UserControl
    {
        public string seriesName { get; set; }
        public int foundEpisodes { get; set; }
        public int foundSeason { get; set; }
        public HitList episodes = new HitList();

        public SearchedSeries(string Name, int e = 0, int s = 0)
        {
            InitializeComponent();
            seriesName = Name;
            foundSeason = s;
            foundEpisodes = e;
        }

        private void btShow_Click(object sender, EventArgs e)
        {
            if (pnlConfig.Visible == false)
            {
                pnlConfig.Visible = true;
                //listBox1.Visible = true;
                //label7.Visible = true;
            }
            else {
                pnlConfig.Visible = false;
                //listBox1.Visible = false;
                //label7.Visible = false;
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pnlConfig.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void SearchedSeries_Load(object sender, EventArgs e)
        {
            btShow.Text = seriesName + " ---->> " + foundEpisodes + " episode' hit(s) | " + foundSeason + " season(s)";
            foreach (Hit h in episodes.Hits) {
                listBox1.Items.Add(h.torrentFileName);
            }

            //verify uif the tv series already exist among the users series and disable some control if true
            if (MomentsInitialization.listOfUsersSeries.Exists(x => x.Name == this.seriesName)) {
                button1.Enabled = false;

                //fetch episode and season number to and display them in the textboxes as proof that they are already saved
                TV_Series show = MomentsInitialization.listOfUsersSeries.Find(x => x.Name == this.seriesName);
                tbSeasonNumber.Text = show.currentSeason.ToString();
                tbEpisodeNumber.Text = show.nextEpisode.ToString();
            }

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pnlConfig_Paint(object sender, PaintEventArgs e)
        {
            if (MomentsInitialization.listOfUsersSeries.Exists(show => show.Name == seriesName))
            {
                button1.Enabled = false;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            pnlConfig.Visible = false;
        }

        private void pnlConfig_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //verify that both values are numbers
            int episodeNumber;
            int seasonNumber;
            int.TryParse(tbEpisodeNumber.Text, out episodeNumber);
            int.TryParse(tbSeasonNumber.Text, out seasonNumber);

            if (tbEpisodeNumber.Text == "#6") {
                label7.Visible = true;
                listBox1.Visible = true;
            }
            else if (episodeNumber == 0 || seasonNumber == 0)
            {
                MessageBox.Show("Please enter number values" + Environment.NewLine + "For example: 01 or 1. Not a zero", "Incorrect numbers", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                //We add to the user favourites, recording the season to look into and the episode to fetch next.
                try
                {
                    //create a tv_series object and save it
                    MomentsInitialization.listOfUsersSeries.Add(new TV_Series(seriesName, seasonNumber, episodeNumber));
                    MomentsInitialization.listOfUsersSeries.Last().addToUserShowList();

                    //File.AppendAllText(MomentsInitialization.path_ShowsFollwoed, seriesName + "#" + seasonNumber + "#" + episodeNumber + "#" + DateTime.Now + Environment.NewLine);
                    //MomentsInitialization.buildListOfUsersSeries();
                    button1.Enabled = false;

                    System.Threading.Tasks.Task t = System.Threading.Tasks.Task.Run(() => MomentsInitialization.addToGeneralListofShows());
                    pnlConfig.Visible = false;
                }
                catch //(Exception ex)
                {
                    MessageBox.Show("An error occured. Please try again later.", "", MessageBoxButtons.OK);
                    button1.Enabled = false;
                }
            }
        }

        private void tbEpisodeNumber_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbSeasonNumber_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
