﻿namespace Moments
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.btHome = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.btPending = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.btManageMoments = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pnlUserAccount = new System.Windows.Forms.Panel();
            this.pnlAccDetails = new System.Windows.Forms.Panel();
            this.accDet_PrefLanguage = new System.Windows.Forms.ComboBox();
            this.accDet_City = new System.Windows.Forms.TextBox();
            this.accDet_MailAddress = new System.Windows.Forms.TextBox();
            this.accDet_Name = new System.Windows.Forms.TextBox();
            this.accDet_ID = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.pnlAccInbox = new System.Windows.Forms.Panel();
            this.button15 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.pnlReady = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlHome = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.pnlCurrentlyDownloading = new System.Windows.Forms.Panel();
            this.lblEmpty = new System.Windows.Forms.Label();
            this.lbRecentDownloads = new System.Windows.Forms.ListBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.lbCurrentlyDownloading = new System.Windows.Forms.ListBox();
            this.btReady = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button11 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.gridShows = new System.Windows.Forms.DataGridView();
            this.pnlSearchedSeriesResult = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlResultsSumary = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlMesssage = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.message_BodyTXT = new System.Windows.Forms.TextBox();
            this.message_ObjectTXT = new System.Windows.Forms.TextBox();
            this.message_TypeTXT = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblUserID = new System.Windows.Forms.Label();
            this.pnlSearch = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlSearchingAnimation = new System.Windows.Forms.Panel();
            this.lblSearchFeedback = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pnlSearchProgressBar = new System.Windows.Forms.Panel();
            this.pnlNotFound = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.pnlPending = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlManageMoments = new System.Windows.Forms.FlowLayoutPanel();
            this.Brand = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.btUpdate = new System.Windows.Forms.Button();
            this.btUserAccount = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.pnlUserAccount.SuspendLayout();
            this.pnlAccDetails.SuspendLayout();
            this.pnlAccInbox.SuspendLayout();
            this.pnlHome.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlCurrentlyDownloading.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridShows)).BeginInit();
            this.pnlSearchedSeriesResult.SuspendLayout();
            this.pnlResultsSumary.SuspendLayout();
            this.pnlMesssage.SuspendLayout();
            this.pnlSearch.SuspendLayout();
            this.pnlSearchingAnimation.SuspendLayout();
            this.panel5.SuspendLayout();
            this.pnlNotFound.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.button8);
            this.panel3.Controls.Add(this.btHome);
            this.panel3.Controls.Add(this.button7);
            this.panel3.Controls.Add(this.btPending);
            this.panel3.Controls.Add(this.tbSearch);
            this.panel3.Controls.Add(this.btManageMoments);
            this.panel3.Location = new System.Drawing.Point(157, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(605, 54);
            this.panel3.TabIndex = 4;
            // 
            // button8
            // 
            this.button8.BackgroundImage = global::Moments.Properties.Resources.wechat;
            this.button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(153, 15);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(26, 26);
            this.button8.TabIndex = 9;
            this.button8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // btHome
            // 
            this.btHome.BackgroundImage = global::Moments.Properties.Resources.home;
            this.btHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btHome.FlatAppearance.BorderSize = 0;
            this.btHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btHome.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btHome.ForeColor = System.Drawing.Color.White;
            this.btHome.Location = new System.Drawing.Point(47, 15);
            this.btHome.Name = "btHome";
            this.btHome.Size = new System.Drawing.Size(26, 26);
            this.btHome.TabIndex = 5;
            this.btHome.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btHome.UseVisualStyleBackColor = true;
            this.btHome.Click += new System.EventHandler(this.button6_Click);
            this.btHome.MouseHover += new System.EventHandler(this.btHome_MouseHover);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.White;
            this.button7.BackgroundImage = global::Moments.Properties.Resources.search;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.button7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button7.Location = new System.Drawing.Point(508, 16);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(26, 22);
            this.button7.TabIndex = 6;
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            this.button7.MouseHover += new System.EventHandler(this.button7_MouseHover);
            // 
            // btPending
            // 
            this.btPending.BackgroundImage = global::Moments.Properties.Resources.pending;
            this.btPending.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btPending.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btPending.FlatAppearance.BorderSize = 0;
            this.btPending.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btPending.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPending.ForeColor = System.Drawing.Color.White;
            this.btPending.Location = new System.Drawing.Point(117, 15);
            this.btPending.Name = "btPending";
            this.btPending.Size = new System.Drawing.Size(26, 26);
            this.btPending.TabIndex = 4;
            this.btPending.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btPending.UseVisualStyleBackColor = true;
            this.btPending.Click += new System.EventHandler(this.btPending_Click);
            this.btPending.MouseHover += new System.EventHandler(this.btPending_MouseHover);
            // 
            // tbSearch
            // 
            this.tbSearch.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.tbSearch.Location = new System.Drawing.Point(185, 15);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(349, 24);
            this.tbSearch.TabIndex = 8;
            this.tbSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSearch_KeyDown);
            // 
            // btManageMoments
            // 
            this.btManageMoments.BackgroundImage = global::Moments.Properties.Resources.animation;
            this.btManageMoments.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btManageMoments.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btManageMoments.FlatAppearance.BorderSize = 0;
            this.btManageMoments.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btManageMoments.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btManageMoments.ForeColor = System.Drawing.Color.White;
            this.btManageMoments.Location = new System.Drawing.Point(79, 14);
            this.btManageMoments.Name = "btManageMoments";
            this.btManageMoments.Size = new System.Drawing.Size(27, 26);
            this.btManageMoments.TabIndex = 2;
            this.btManageMoments.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btManageMoments.UseVisualStyleBackColor = true;
            this.btManageMoments.Click += new System.EventHandler(this.btManageMoments_Click);
            this.btManageMoments.MouseHover += new System.EventHandler(this.button5_MouseHover);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Gray;
            this.panel4.Controls.Add(this.pnlUserAccount);
            this.panel4.Controls.Add(this.pnlReady);
            this.panel4.Controls.Add(this.pnlHome);
            this.panel4.Controls.Add(this.panel3);
            this.panel4.Controls.Add(this.pnlSearchedSeriesResult);
            this.panel4.Controls.Add(this.pnlMesssage);
            this.panel4.Controls.Add(this.lblUserID);
            this.panel4.Controls.Add(this.pnlSearch);
            this.panel4.Controls.Add(this.pnlPending);
            this.panel4.Controls.Add(this.pnlManageMoments);
            this.panel4.Controls.Add(this.Brand);
            this.panel4.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(7, 33);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(854, 374);
            this.panel4.TabIndex = 5;
            // 
            // pnlUserAccount
            // 
            this.pnlUserAccount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.pnlUserAccount.Controls.Add(this.pnlAccDetails);
            this.pnlUserAccount.Controls.Add(this.pnlAccInbox);
            this.pnlUserAccount.Controls.Add(this.button13);
            this.pnlUserAccount.Controls.Add(this.button12);
            this.pnlUserAccount.Controls.Add(this.button3);
            this.pnlUserAccount.Controls.Add(this.label16);
            this.pnlUserAccount.Location = new System.Drawing.Point(7, 239);
            this.pnlUserAccount.Name = "pnlUserAccount";
            this.pnlUserAccount.Size = new System.Drawing.Size(605, 293);
            this.pnlUserAccount.TabIndex = 3;
            this.pnlUserAccount.Visible = false;
            // 
            // pnlAccDetails
            // 
            this.pnlAccDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.pnlAccDetails.Controls.Add(this.accDet_PrefLanguage);
            this.pnlAccDetails.Controls.Add(this.accDet_City);
            this.pnlAccDetails.Controls.Add(this.accDet_MailAddress);
            this.pnlAccDetails.Controls.Add(this.accDet_Name);
            this.pnlAccDetails.Controls.Add(this.accDet_ID);
            this.pnlAccDetails.Controls.Add(this.label15);
            this.pnlAccDetails.Controls.Add(this.label14);
            this.pnlAccDetails.Controls.Add(this.label13);
            this.pnlAccDetails.Controls.Add(this.label12);
            this.pnlAccDetails.Controls.Add(this.label9);
            this.pnlAccDetails.Controls.Add(this.button17);
            this.pnlAccDetails.Controls.Add(this.button16);
            this.pnlAccDetails.Controls.Add(this.button14);
            this.pnlAccDetails.Location = new System.Drawing.Point(162, 116);
            this.pnlAccDetails.Name = "pnlAccDetails";
            this.pnlAccDetails.Size = new System.Drawing.Size(443, 283);
            this.pnlAccDetails.TabIndex = 14;
            this.pnlAccDetails.Visible = false;
            // 
            // accDet_PrefLanguage
            // 
            this.accDet_PrefLanguage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.accDet_PrefLanguage.Enabled = false;
            this.accDet_PrefLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.accDet_PrefLanguage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.accDet_PrefLanguage.FormattingEnabled = true;
            this.accDet_PrefLanguage.IntegralHeight = false;
            this.accDet_PrefLanguage.Items.AddRange(new object[] {
            "English",
            "French",
            "German",
            "Spanish",
            "Russian"});
            this.accDet_PrefLanguage.Location = new System.Drawing.Point(236, 181);
            this.accDet_PrefLanguage.Name = "accDet_PrefLanguage";
            this.accDet_PrefLanguage.Size = new System.Drawing.Size(148, 25);
            this.accDet_PrefLanguage.TabIndex = 24;
            // 
            // accDet_City
            // 
            this.accDet_City.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.accDet_City.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.accDet_City.Enabled = false;
            this.accDet_City.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accDet_City.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.accDet_City.Location = new System.Drawing.Point(236, 151);
            this.accDet_City.Name = "accDet_City";
            this.accDet_City.Size = new System.Drawing.Size(148, 17);
            this.accDet_City.TabIndex = 23;
            // 
            // accDet_MailAddress
            // 
            this.accDet_MailAddress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.accDet_MailAddress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.accDet_MailAddress.Enabled = false;
            this.accDet_MailAddress.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accDet_MailAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.accDet_MailAddress.Location = new System.Drawing.Point(235, 122);
            this.accDet_MailAddress.Name = "accDet_MailAddress";
            this.accDet_MailAddress.Size = new System.Drawing.Size(148, 17);
            this.accDet_MailAddress.TabIndex = 22;
            // 
            // accDet_Name
            // 
            this.accDet_Name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.accDet_Name.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.accDet_Name.Enabled = false;
            this.accDet_Name.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accDet_Name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.accDet_Name.Location = new System.Drawing.Point(235, 92);
            this.accDet_Name.Name = "accDet_Name";
            this.accDet_Name.Size = new System.Drawing.Size(148, 17);
            this.accDet_Name.TabIndex = 21;
            // 
            // accDet_ID
            // 
            this.accDet_ID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.accDet_ID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.accDet_ID.Enabled = false;
            this.accDet_ID.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accDet_ID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.accDet_ID.Location = new System.Drawing.Point(236, 64);
            this.accDet_ID.Name = "accDet_ID";
            this.accDet_ID.Size = new System.Drawing.Size(148, 17);
            this.accDet_ID.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(114, 184);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(116, 17);
            this.label15.TabIndex = 19;
            this.label15.Text = "Preferred language:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(195, 157);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 17);
            this.label14.TabIndex = 18;
            this.label14.Text = "City:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(148, 122);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 17);
            this.label13.TabIndex = 17;
            this.label13.Text = "Mail address:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(187, 92);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 17);
            this.label12.TabIndex = 16;
            this.label12.Text = "Name:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(94, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 17);
            this.label9.TabIndex = 15;
            this.label9.Text = "Your account(user) ID:";
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button17.FlatAppearance.BorderSize = 0;
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.ForeColor = System.Drawing.Color.White;
            this.button17.Location = new System.Drawing.Point(236, 240);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(82, 25);
            this.button17.TabIndex = 14;
            this.button17.Text = "save";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button16.FlatAppearance.BorderSize = 0;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.ForeColor = System.Drawing.Color.White;
            this.button16.Location = new System.Drawing.Point(148, 240);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(82, 25);
            this.button16.TabIndex = 13;
            this.button16.Text = "edit";
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button14.Enabled = false;
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ForeColor = System.Drawing.Color.Gray;
            this.button14.Location = new System.Drawing.Point(3, 3);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(437, 31);
            this.button14.TabIndex = 12;
            this.button14.Text = "Account details";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // pnlAccInbox
            // 
            this.pnlAccInbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.pnlAccInbox.Controls.Add(this.button15);
            this.pnlAccInbox.Location = new System.Drawing.Point(109, 126);
            this.pnlAccInbox.Name = "pnlAccInbox";
            this.pnlAccInbox.Size = new System.Drawing.Size(443, 283);
            this.pnlAccInbox.TabIndex = 15;
            this.pnlAccInbox.Visible = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button15.Enabled = false;
            this.button15.FlatAppearance.BorderSize = 0;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.ForeColor = System.Drawing.Color.Gray;
            this.button15.Location = new System.Drawing.Point(3, 3);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(437, 31);
            this.button15.TabIndex = 12;
            this.button15.Text = "Inbox";
            this.button15.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Gray;
            this.button13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button13.Location = new System.Drawing.Point(3, 76);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(150, 26);
            this.button13.TabIndex = 13;
            this.button13.Text = "Deactivate account";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Gray;
            this.button12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button12.Location = new System.Drawing.Point(3, 44);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(150, 26);
            this.button12.TabIndex = 12;
            this.button12.Text = "Message Box";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Gray;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button3.Location = new System.Drawing.Point(3, 11);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(150, 26);
            this.button3.TabIndex = 11;
            this.button3.Text = "Account details";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(203, 51);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(343, 85);
            this.label16.TabIndex = 16;
            this.label16.Text = "To all our users,\r\nPlease note that ; \r\nTulz, the software company behind this to" +
    "ol does not collect\r\nuser\'s data. We do not save even your application\'s setting" +
    "s\r\non our database. ";
            // 
            // pnlReady
            // 
            this.pnlReady.AutoScroll = true;
            this.pnlReady.Location = new System.Drawing.Point(246, 104);
            this.pnlReady.Name = "pnlReady";
            this.pnlReady.Size = new System.Drawing.Size(605, 293);
            this.pnlReady.TabIndex = 8;
            this.pnlReady.Visible = false;
            // 
            // pnlHome
            // 
            this.pnlHome.AutoScroll = true;
            this.pnlHome.BackColor = System.Drawing.Color.Gray;
            this.pnlHome.Controls.Add(this.panel1);
            this.pnlHome.Controls.Add(this.pnlCurrentlyDownloading);
            this.pnlHome.Controls.Add(this.panel2);
            this.pnlHome.Location = new System.Drawing.Point(30, 62);
            this.pnlHome.Name = "pnlHome";
            this.pnlHome.Size = new System.Drawing.Size(794, 293);
            this.pnlHome.TabIndex = 7;
            this.pnlHome.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlHome_Paint);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(389, 131);
            this.panel1.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(14, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(361, 85);
            this.label10.TabIndex = 6;
            this.label10.Text = resources.GetString("label10.Text");
            // 
            // pnlCurrentlyDownloading
            // 
            this.pnlCurrentlyDownloading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.pnlCurrentlyDownloading.Controls.Add(this.lblEmpty);
            this.pnlCurrentlyDownloading.Controls.Add(this.lbRecentDownloads);
            this.pnlCurrentlyDownloading.Controls.Add(this.button10);
            this.pnlCurrentlyDownloading.Controls.Add(this.button9);
            this.pnlCurrentlyDownloading.Controls.Add(this.lbCurrentlyDownloading);
            this.pnlCurrentlyDownloading.Controls.Add(this.btReady);
            this.pnlCurrentlyDownloading.Location = new System.Drawing.Point(398, 3);
            this.pnlCurrentlyDownloading.Name = "pnlCurrentlyDownloading";
            this.pnlCurrentlyDownloading.Size = new System.Drawing.Size(389, 131);
            this.pnlCurrentlyDownloading.TabIndex = 1;
            // 
            // lblEmpty
            // 
            this.lblEmpty.AutoSize = true;
            this.lblEmpty.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpty.ForeColor = System.Drawing.Color.White;
            this.lblEmpty.Location = new System.Drawing.Point(155, 64);
            this.lblEmpty.Name = "lblEmpty";
            this.lblEmpty.Size = new System.Drawing.Size(118, 17);
            this.lblEmpty.TabIndex = 5;
            this.lblEmpty.Text = "no current download";
            // 
            // lbRecentDownloads
            // 
            this.lbRecentDownloads.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.lbRecentDownloads.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbRecentDownloads.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRecentDownloads.ForeColor = System.Drawing.SystemColors.Window;
            this.lbRecentDownloads.FormattingEnabled = true;
            this.lbRecentDownloads.ItemHeight = 15;
            this.lbRecentDownloads.Location = new System.Drawing.Point(3, 32);
            this.lbRecentDownloads.Name = "lbRecentDownloads";
            this.lbRecentDownloads.Size = new System.Drawing.Size(383, 90);
            this.lbRecentDownloads.TabIndex = 12;
            this.lbRecentDownloads.Visible = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.Gray;
            this.button10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Comic Sans MS", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button10.Location = new System.Drawing.Point(220, 5);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(150, 26);
            this.button10.TabIndex = 11;
            this.button10.Text = "Recent downloads";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Gray;
            this.button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Comic Sans MS", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button9.Location = new System.Drawing.Point(51, 5);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(150, 26);
            this.button9.TabIndex = 10;
            this.button9.Text = "Currently downloading";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // lbCurrentlyDownloading
            // 
            this.lbCurrentlyDownloading.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.lbCurrentlyDownloading.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbCurrentlyDownloading.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentlyDownloading.ForeColor = System.Drawing.SystemColors.Window;
            this.lbCurrentlyDownloading.FormattingEnabled = true;
            this.lbCurrentlyDownloading.ItemHeight = 15;
            this.lbCurrentlyDownloading.Location = new System.Drawing.Point(3, 32);
            this.lbCurrentlyDownloading.Name = "lbCurrentlyDownloading";
            this.lbCurrentlyDownloading.Size = new System.Drawing.Size(383, 90);
            this.lbCurrentlyDownloading.TabIndex = 4;
            // 
            // btReady
            // 
            this.btReady.BackgroundImage = global::Moments.Properties.Resources.ready;
            this.btReady.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btReady.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btReady.FlatAppearance.BorderSize = 0;
            this.btReady.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btReady.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btReady.ForeColor = System.Drawing.Color.White;
            this.btReady.Location = new System.Drawing.Point(18, 5);
            this.btReady.Name = "btReady";
            this.btReady.Size = new System.Drawing.Size(27, 26);
            this.btReady.TabIndex = 3;
            this.btReady.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btReady.UseVisualStyleBackColor = true;
            this.btReady.Click += new System.EventHandler(this.btReady_Click);
            this.btReady.MouseHover += new System.EventHandler(this.btReady_MouseHover);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            this.panel2.Controls.Add(this.button11);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.gridShows);
            this.panel2.Location = new System.Drawing.Point(3, 140);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(784, 150);
            this.panel2.TabIndex = 2;
            // 
            // button11
            // 
            this.button11.BackgroundImage = global::Moments.Properties.Resources.refresh1;
            this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.Location = new System.Drawing.Point(488, 4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(16, 15);
            this.button11.TabIndex = 9;
            this.button11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(322, 1);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(169, 17);
            this.label11.TabIndex = 6;
            this.label11.Text = "Other Moments users\' shows";
            // 
            // gridShows
            // 
            this.gridShows.AllowUserToAddRows = false;
            this.gridShows.AllowUserToDeleteRows = false;
            this.gridShows.AllowUserToOrderColumns = true;
            this.gridShows.AllowUserToResizeRows = false;
            this.gridShows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.gridShows.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridShows.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.gridShows.BackgroundColor = System.Drawing.Color.Gray;
            this.gridShows.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridShows.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gridShows.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.gridShows.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.NullValue = "0";
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DarkViolet;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridShows.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridShows.GridColor = System.Drawing.Color.Gray;
            this.gridShows.Location = new System.Drawing.Point(3, 21);
            this.gridShows.Name = "gridShows";
            this.gridShows.RowHeadersVisible = false;
            this.gridShows.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gridShows.Size = new System.Drawing.Size(778, 126);
            this.gridShows.TabIndex = 8;
            // 
            // pnlSearchedSeriesResult
            // 
            this.pnlSearchedSeriesResult.AutoScroll = true;
            this.pnlSearchedSeriesResult.Controls.Add(this.pnlResultsSumary);
            this.pnlSearchedSeriesResult.Location = new System.Drawing.Point(125, 41);
            this.pnlSearchedSeriesResult.Name = "pnlSearchedSeriesResult";
            this.pnlSearchedSeriesResult.Size = new System.Drawing.Size(605, 293);
            this.pnlSearchedSeriesResult.TabIndex = 12;
            this.pnlSearchedSeriesResult.Visible = false;
            // 
            // pnlResultsSumary
            // 
            this.pnlResultsSumary.Controls.Add(this.label8);
            this.pnlResultsSumary.Controls.Add(this.label7);
            this.pnlResultsSumary.Location = new System.Drawing.Point(3, 3);
            this.pnlResultsSumary.Name = "pnlResultsSumary";
            this.pnlResultsSumary.Size = new System.Drawing.Size(594, 28);
            this.pnlResultsSumary.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Comic Sans MS", 7.75F);
            this.label8.Location = new System.Drawing.Point(273, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(180, 15);
            this.label8.TabIndex = 1;
            this.label8.Text = "differents source/tv shows found";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Comic Sans MS", 7.75F);
            this.label7.Location = new System.Drawing.Point(226, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "label7";
            // 
            // pnlMesssage
            // 
            this.pnlMesssage.Controls.Add(this.button6);
            this.pnlMesssage.Controls.Add(this.button5);
            this.pnlMesssage.Controls.Add(this.label6);
            this.pnlMesssage.Controls.Add(this.label5);
            this.pnlMesssage.Controls.Add(this.label4);
            this.pnlMesssage.Controls.Add(this.message_BodyTXT);
            this.pnlMesssage.Controls.Add(this.message_ObjectTXT);
            this.pnlMesssage.Controls.Add(this.message_TypeTXT);
            this.pnlMesssage.Controls.Add(this.label3);
            this.pnlMesssage.Location = new System.Drawing.Point(5, 358);
            this.pnlMesssage.Name = "pnlMesssage";
            this.pnlMesssage.Size = new System.Drawing.Size(605, 293);
            this.pnlMesssage.TabIndex = 10;
            this.pnlMesssage.Visible = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(480, 226);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(76, 24);
            this.button6.TabIndex = 8;
            this.button6.Text = "cancel";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(392, 226);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(76, 24);
            this.button5.TabIndex = 7;
            this.button5.Text = "send";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.label6.Location = new System.Drawing.Point(185, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Message";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.label5.Location = new System.Drawing.Point(185, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Object";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 9F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.label4.Location = new System.Drawing.Point(198, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Type";
            // 
            // message_BodyTXT
            // 
            this.message_BodyTXT.Location = new System.Drawing.Point(238, 97);
            this.message_BodyTXT.Multiline = true;
            this.message_BodyTXT.Name = "message_BodyTXT";
            this.message_BodyTXT.Size = new System.Drawing.Size(318, 123);
            this.message_BodyTXT.TabIndex = 3;
            // 
            // message_ObjectTXT
            // 
            this.message_ObjectTXT.Location = new System.Drawing.Point(238, 67);
            this.message_ObjectTXT.Name = "message_ObjectTXT";
            this.message_ObjectTXT.Size = new System.Drawing.Size(318, 24);
            this.message_ObjectTXT.TabIndex = 2;
            // 
            // message_TypeTXT
            // 
            this.message_TypeTXT.FormattingEnabled = true;
            this.message_TypeTXT.Items.AddRange(new object[] {
            "Content Suggestion",
            "Bug Report",
            "Chat with us"});
            this.message_TypeTXT.Location = new System.Drawing.Point(238, 32);
            this.message_TypeTXT.Name = "message_TypeTXT";
            this.message_TypeTXT.Size = new System.Drawing.Size(318, 25);
            this.message_TypeTXT.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 12F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.label3.Location = new System.Drawing.Point(307, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(212, 23);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tell us what\'s on your mind!";
            // 
            // lblUserID
            // 
            this.lblUserID.AutoSize = true;
            this.lblUserID.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserID.ForeColor = System.Drawing.Color.Silver;
            this.lblUserID.Location = new System.Drawing.Point(5, 0);
            this.lblUserID.Name = "lblUserID";
            this.lblUserID.Size = new System.Drawing.Size(96, 12);
            this.lblUserID.TabIndex = 11;
            this.lblUserID.Text = "User ID: (collecting...)";
            // 
            // pnlSearch
            // 
            this.pnlSearch.AutoScroll = true;
            this.pnlSearch.AutoSize = true;
            this.pnlSearch.Controls.Add(this.pnlSearchingAnimation);
            this.pnlSearch.Controls.Add(this.pnlNotFound);
            this.pnlSearch.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.pnlSearch.Location = new System.Drawing.Point(30, 15);
            this.pnlSearch.MaximumSize = new System.Drawing.Size(605, 293);
            this.pnlSearch.MinimumSize = new System.Drawing.Size(605, 293);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(605, 293);
            this.pnlSearch.TabIndex = 8;
            this.pnlSearch.Visible = false;
            // 
            // pnlSearchingAnimation
            // 
            this.pnlSearchingAnimation.Controls.Add(this.lblSearchFeedback);
            this.pnlSearchingAnimation.Controls.Add(this.panel5);
            this.pnlSearchingAnimation.Location = new System.Drawing.Point(3, 3);
            this.pnlSearchingAnimation.Name = "pnlSearchingAnimation";
            this.pnlSearchingAnimation.Size = new System.Drawing.Size(591, 50);
            this.pnlSearchingAnimation.TabIndex = 5;
            this.pnlSearchingAnimation.Visible = false;
            // 
            // lblSearchFeedback
            // 
            this.lblSearchFeedback.AutoSize = true;
            this.lblSearchFeedback.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSearchFeedback.ForeColor = System.Drawing.Color.White;
            this.lblSearchFeedback.Location = new System.Drawing.Point(275, 5);
            this.lblSearchFeedback.Name = "lblSearchFeedback";
            this.lblSearchFeedback.Size = new System.Drawing.Size(75, 16);
            this.lblSearchFeedback.TabIndex = 5;
            this.lblSearchFeedback.Text = "Searching...";
            this.lblSearchFeedback.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel5.Controls.Add(this.pnlSearchProgressBar);
            this.panel5.Location = new System.Drawing.Point(100, 24);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(423, 16);
            this.panel5.TabIndex = 7;
            // 
            // pnlSearchProgressBar
            // 
            this.pnlSearchProgressBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.pnlSearchProgressBar.Location = new System.Drawing.Point(1, 3);
            this.pnlSearchProgressBar.Name = "pnlSearchProgressBar";
            this.pnlSearchProgressBar.Size = new System.Drawing.Size(422, 10);
            this.pnlSearchProgressBar.TabIndex = 6;
            // 
            // pnlNotFound
            // 
            this.pnlNotFound.Controls.Add(this.label1);
            this.pnlNotFound.Controls.Add(this.button4);
            this.pnlNotFound.Location = new System.Drawing.Point(3, 59);
            this.pnlNotFound.Name = "pnlNotFound";
            this.pnlNotFound.Size = new System.Drawing.Size(591, 77);
            this.pnlNotFound.TabIndex = 4;
            this.pnlNotFound.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 8.25F);
            this.label1.Location = new System.Drawing.Point(61, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(487, 30);
            this.label1.TabIndex = 4;
            this.label1.Text = "Cannot find the TV Series you are looking for? Don\'t worry, just clik on the requ" +
    "est button.\r\nWe will include the series or movie in the featured content as soon" +
    " as we process your request";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(145)))));
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(233, 33);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(106, 34);
            this.button4.TabIndex = 3;
            this.button4.Text = "request";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // pnlPending
            // 
            this.pnlPending.AutoScroll = true;
            this.pnlPending.Location = new System.Drawing.Point(688, 361);
            this.pnlPending.Name = "pnlPending";
            this.pnlPending.Size = new System.Drawing.Size(605, 293);
            this.pnlPending.TabIndex = 9;
            this.pnlPending.Visible = false;
            // 
            // pnlManageMoments
            // 
            this.pnlManageMoments.AutoScroll = true;
            this.pnlManageMoments.Location = new System.Drawing.Point(517, 62);
            this.pnlManageMoments.Name = "pnlManageMoments";
            this.pnlManageMoments.Size = new System.Drawing.Size(605, 293);
            this.pnlManageMoments.TabIndex = 9;
            this.pnlManageMoments.Visible = false;
            // 
            // Brand
            // 
            this.Brand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Brand.AutoSize = true;
            this.Brand.Font = new System.Drawing.Font("Comic Sans MS", 7F);
            this.Brand.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.Brand.Location = new System.Drawing.Point(380, 358);
            this.Brand.Name = "Brand";
            this.Brand.Size = new System.Drawing.Size(168, 14);
            this.Brand.TabIndex = 6;
            this.Brand.Text = "Nehemie\'s Tasiz © Copyright 2017";
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 50;
            this.toolTip1.ShowAlways = true;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Moments";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 12F);
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(7, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 23);
            this.label2.TabIndex = 6;
            this.label2.Text = "Moments";
            this.label2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.label2_MouseClick);
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F);
            this.lblVersion.ForeColor = System.Drawing.Color.Gray;
            this.lblVersion.Location = new System.Drawing.Point(76, 18);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(25, 9);
            this.lblVersion.TabIndex = 7;
            this.lblVersion.Text = "v0.0.0";
            // 
            // btUpdate
            // 
            this.btUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btUpdate.FlatAppearance.BorderSize = 0;
            this.btUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btUpdate.Image = global::Moments.Properties.Resources.update;
            this.btUpdate.Location = new System.Drawing.Point(109, 4);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(25, 27);
            this.btUpdate.TabIndex = 8;
            this.btUpdate.UseVisualStyleBackColor = false;
            this.btUpdate.Visible = false;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // btUserAccount
            // 
            this.btUserAccount.BackColor = System.Drawing.Color.Transparent;
            this.btUserAccount.FlatAppearance.BorderSize = 0;
            this.btUserAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btUserAccount.Image = global::Moments.Properties.Resources.account;
            this.btUserAccount.Location = new System.Drawing.Point(778, 2);
            this.btUserAccount.Name = "btUserAccount";
            this.btUserAccount.Size = new System.Drawing.Size(27, 27);
            this.btUserAccount.TabIndex = 2;
            this.btUserAccount.UseVisualStyleBackColor = false;
            this.btUserAccount.Visible = false;
            this.btUserAccount.Click += new System.EventHandler(this.btUserAccount_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = global::Moments.Properties.Resources.window_minimize;
            this.button2.Location = new System.Drawing.Point(804, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(27, 27);
            this.button2.TabIndex = 1;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = global::Moments.Properties.Resources.close_circle_outline__2_;
            this.button1.Location = new System.Drawing.Point(832, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 27);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(870, 419);
            this.Controls.Add(this.btUpdate);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.btUserAccount);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(870, 419);
            this.MinimizeBox = false;
            this.Name = "Home";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home";
            this.Load += new System.EventHandler(this.Home_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Home_MouseDown);
            this.Move += new System.EventHandler(this.Home_Move);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.pnlUserAccount.ResumeLayout(false);
            this.pnlUserAccount.PerformLayout();
            this.pnlAccDetails.ResumeLayout(false);
            this.pnlAccDetails.PerformLayout();
            this.pnlAccInbox.ResumeLayout(false);
            this.pnlHome.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlCurrentlyDownloading.ResumeLayout(false);
            this.pnlCurrentlyDownloading.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridShows)).EndInit();
            this.pnlSearchedSeriesResult.ResumeLayout(false);
            this.pnlResultsSumary.ResumeLayout(false);
            this.pnlResultsSumary.PerformLayout();
            this.pnlMesssage.ResumeLayout(false);
            this.pnlMesssage.PerformLayout();
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearchingAnimation.ResumeLayout(false);
            this.pnlSearchingAnimation.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.pnlNotFound.ResumeLayout(false);
            this.pnlNotFound.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btReady;
        private System.Windows.Forms.Button btPending;
        private System.Windows.Forms.Button btHome;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label Brand;
        private System.Windows.Forms.FlowLayoutPanel pnlHome;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.FlowLayoutPanel pnlSearch;
        public System.Windows.Forms.Button btManageMoments;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel pnlNotFound;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        public System.Windows.Forms.FlowLayoutPanel pnlReady;
        public System.Windows.Forms.FlowLayoutPanel pnlPending;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Panel pnlMesssage;
        public System.Windows.Forms.Button button6;
        public System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox message_BodyTXT;
        private System.Windows.Forms.TextBox message_ObjectTXT;
        private System.Windows.Forms.ComboBox message_TypeTXT;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button8;
        public System.Windows.Forms.Label lblUserID;
        private System.Windows.Forms.Panel pnlSearchingAnimation;
        private System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Panel pnlSearchProgressBar;
        public System.Windows.Forms.Label lblSearchFeedback;
        private System.Windows.Forms.FlowLayoutPanel pnlSearchedSeriesResult;
        private System.Windows.Forms.Panel pnlResultsSumary;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlCurrentlyDownloading;
        private System.Windows.Forms.ListBox lbCurrentlyDownloading;
        private System.Windows.Forms.Label lblEmpty;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.DataGridView gridShows;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        public System.Windows.Forms.ListBox lbRecentDownloads;
        private System.Windows.Forms.Button button11;
        public System.Windows.Forms.TextBox tbSearch;
        public System.Windows.Forms.FlowLayoutPanel pnlManageMoments;
        public System.Windows.Forms.Panel pnlUserAccount;
        public System.Windows.Forms.Button btUserAccount;
        public System.Windows.Forms.Panel pnlAccDetails;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.Panel pnlAccInbox;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox accDet_PrefLanguage;
        public System.Windows.Forms.TextBox accDet_City;
        public System.Windows.Forms.TextBox accDet_MailAddress;
        public System.Windows.Forms.TextBox accDet_Name;
        public System.Windows.Forms.TextBox accDet_ID;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.Button btUpdate;
        private System.Windows.Forms.Label label16;
    }
}

