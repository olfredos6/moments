﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Moments
{
    public partial class Download : UserControl
    {
        public string TVSeries_Name { get; set; }
        public string downloadState { get; set;}
        public Download(string seriesTitle, double progression, string state)
        {
            InitializeComponent();
            lblTitle.Text = seriesTitle;
            TVSeries_Name = seriesTitle;
            this.Name = seriesTitle;
            downloadState = state;

            panel2.Width = Convert.ToInt16(progression * (panel1.Width / 100));
        }

        private void Download_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            buttonDisplayManagement(sender as Button);
            DownloadManager._torrents[DownloadManager._torrents.FindIndex(x => x.Torrent.Name == this.Name)].Start();
        }

        delegate void changeProgressBarSizeCallBack(double progression, string state);
        public void changeProgressBarSize(double progression, string state) {
            if (panel2.InvokeRequired) {
                changeProgressBarSizeCallBack d = new changeProgressBarSizeCallBack(changeProgressBarSize);
                this.Invoke(d, new object[] { progression, state });
            }
            else {
                panel2.Width = Convert.ToInt16((progression * panel1.Width) /100);
                //////////////(Convert.ToInt16((percent * this.Width) / 100))
                //panel2.Width = Convert.ToInt16((panel1.Width * progression));
            }

            if (lblStatus.InvokeRequired)
            {
                changeProgressBarSizeCallBack d = new changeProgressBarSizeCallBack(changeProgressBarSize);
                try { this.Invoke(d, new object[] { progression, state }); } catch { }
            }
            else
            {
                lblStatus.Text = state;
            }
        }

        private void btHome_Click(object sender, EventArgs e)
        {
            buttonDisplayManagement(sender as Button);
            DownloadManager._torrents[DownloadManager._torrents.FindIndex(x => x.Torrent.Name == this.Name)].Stop();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            buttonDisplayManagement(sender as Button);
            DownloadManager._torrents[DownloadManager._torrents.FindIndex(x => x.Torrent.Name == this.Name)].Pause();
        }

        private void buttonDisplayManagement(Button clickedButton) {

            if (clickedButton.Name == "btStart")
            {
                btPause.Enabled = true;
                btStop.Enabled = true;
            }
            else {
                btPause.Enabled = true;
                btStop.Enabled = true;
                btStart.Enabled = true;
            }
            clickedButton.Enabled = false;
        }
    }
}
