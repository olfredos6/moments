﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Configuration;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO;



namespace Moments
{
    public partial class Home : Form
    {

        string lastPanel = string.Empty; //Sessioning, keep track of current panel shown
        public Home()
        {
            InitializeComponent();

            lastPanel = "pnlHome";
        }

        private void Home_Load(object sender, EventArgs e)
        {
            lblVersion.Text = "v" + MomentsInitialization.appCurrrentVersion;
            UpdateChecker.thread_checkForUpdate.Start();
        }

        #region Threads of the front page(pnlHome)

        public Thread thread_CurrentlyDownloading = new Thread(getCurrentlyDownloading);

        public static void getCurrentlyDownloading()
        {
            int t = 0; //This number will help refresh the list of recently downloaded shows episode instead of doing it every second

            while (MomentsInitialization.appIsRunning)
            {
                try
                {

                    t++;
                    //Here we populate an refresh the list of latest downloaded episodes
                    if ((t > 25 && t % 25 == 0) || t == 1)
                    {
                        List<string> readyLines = new List<string>();

                        foreach (string line in File.ReadLines(MomentsInitialization.path_readyMomentsFile))
                        {
                            if (line != string.Empty)
                                readyLines.Add(line);
                        }
                        foreach (string line in readyLines.ToList())
                        {
                            if (!(readyLines.Count <= 6))
                            {
                                readyLines.Remove(line);
                            }
                            else
                            {
                                break;
                            }
                        }
                        MomentsInitialization.frmHome.lbRecentDownloads.Invoke((MethodInvoker)(() =>
                                                                     MomentsInitialization.frmHome.lbRecentDownloads.Items.Clear()));
                        foreach (string line in readyLines.ToList())
                        {
                            MomentsInitialization.frmHome.lbRecentDownloads.Invoke((MethodInvoker)(() =>
                            MomentsInitialization.frmHome.lbRecentDownloads.Items.Add(line)));
                        }
                    }


                    //CURRENTLY DOWNLOADING
                    if (DownloadManager._torrents.Count == 0)
                    {
                        //MomentsInitialization.frmHome.lblEmpty.Invoke(new Action(() => MomentsInitialization.frmHome.lblEmpty.Visible = true));
                        MomentsInitialization.frmHome.lbCurrentlyDownloading.Invoke((MethodInvoker)(() => MomentsInitialization.frmHome.lbCurrentlyDownloading.Items.Clear()));
                    }
                    else
                    {

                        MomentsInitialization.frmHome.lblEmpty.Invoke(new Action(() => MomentsInitialization.frmHome.lblEmpty.Visible = false));
                        MomentsInitialization.frmHome.lblSearchFeedback.Invoke((MethodInvoker)(() => MomentsInitialization.frmHome.lbCurrentlyDownloading.Items.Clear()));
                        foreach (OctoTorrent.Client.TorrentManager manager in DownloadManager._torrents)
                        {
                            if (manager.State == OctoTorrent.Common.TorrentState.Downloading && manager.Monitor.DownloadSpeed > 0)
                            {
                                MomentsInitialization.frmHome.lblSearchFeedback.Invoke((MethodInvoker)(() => MomentsInitialization.frmHome.lbCurrentlyDownloading.Items.Add(manager.Torrent.Name.Substring(0, 40) + "... " + string.Format("{0:0.00}kB/sec", manager.Monitor.DownloadSpeed / 1024))));
                            }
                            else if (manager.State == OctoTorrent.Common.TorrentState.Metadata)
                            {
                                MomentsInitialization.frmHome.lblSearchFeedback.Invoke((MethodInvoker)(() => MomentsInitialization.frmHome.lbCurrentlyDownloading.Items.Add(manager.Torrent.Name.Substring(0, 40) + "... " + "getting metadata")));
                            }
                            else
                            {
                                MomentsInitialization.frmHome.lblSearchFeedback.Invoke((MethodInvoker)(() => MomentsInitialization.frmHome.lbCurrentlyDownloading.Items.Add(manager.Torrent.Name.Substring(0, 40) + "... " + manager.State)));
                            }
                        }
                    }
                }
                catch { }
                Thread.Sleep(1000);
            }


        }

        #endregion

        #region SEARCH
        public int numberOfPageScrapped = 0; //this serves to help the loading animation
        public void search()
        {

            pnlSearchProgressBar.Width = 0;
            if (!string.IsNullOrWhiteSpace(tbSearch.Text) || !string.IsNullOrEmpty(tbSearch.Text))
            {
                //TV_Series resultTVSeries = new TV_Series();

                pnlSearchedSeriesResult.Visible = false;
                pnlNotFound.Visible = false;
                pnlSearchingAnimation.Visible = true;


                HitList ListOfHits = HitList.SearchSeries(tbSearch.Text.Replace("'", ""));
                List<SearchedSeries> listOfSeriesFoundControls = new List<SearchedSeries>();
                //resultTVSeries = Search.search(tbSearch.Text);

                if (ListOfHits != null)
                {
                    int indexAtListOfControls = 0;
                    //hidePanels(pnlSearch);
                    foreach (Hit hitEpisode in ListOfHits.Hits)
                    {
                        try
                        {
                            indexAtListOfControls = listOfSeriesFoundControls.IndexOf(listOfSeriesFoundControls.Find(x => x.seriesName == hitEpisode.seriesName));

                            listOfSeriesFoundControls[indexAtListOfControls].foundEpisodes++;
                        }
                        catch
                        {
                            listOfSeriesFoundControls.Add(new SearchedSeries(hitEpisode.seriesName, 1, ListOfHits.getNumberOfSeason(hitEpisode.seriesName)));
                        }
                        listOfSeriesFoundControls.Find(x => x.seriesName == hitEpisode.seriesName).episodes.Hits.Add(hitEpisode);
                    }

                    pnlSearchedSeriesResult.Controls.Clear();
                    pnlSearchedSeriesResult.Visible = true;
                    pnlNotFound.Visible = false;
                    pnlSearchingAnimation.Visible = false;

                    foreach (SearchedSeries control in listOfSeriesFoundControls)
                    {
                        pnlSearchedSeriesResult.Controls.Add(control);
                    }

                    hidePanels(pnlSearchedSeriesResult);

                    label7.Text = listOfSeriesFoundControls.Count.ToString();
                    pnlResultsSumary.Visible = true;
                }
                else
                {
                    //Display the panel for requesting a the TV Series to be featured
                    pnlSearchingAnimation.Visible = false;
                    pnlSearchedSeriesResult.Visible = false;
                    pnlNotFound.Visible = true;
                    pnlSearch.Controls.Add(pnlNotFound);
                    pnlNotFound.Location = new Point(3, 3);
                }
            }
            else
            {

                hidePanels(panel4.Controls.Find(lastPanel, true).FirstOrDefault() as Panel);
            }
        }
        #endregion

        #region AUTO-GENERATED METHODS and then modified(Implemented)


        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            MomentsInitialization.appIsRunning = false;
            Application.Exit();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            this.ShowInTaskbar = false;
        }

        private void tbSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                hidePanels(pnlSearch);
                search();
            }
        }


        private void button7_Click(object sender, EventArgs e)
        {
            hidePanels(pnlSearch);
            search();

        }
        private void button6_Click(object sender, EventArgs e)
        {
            //loadFeturedSeries();
            hidePanels(pnlHome);
        }

        private void btHome_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btHome, "Go back to home");
        }

        private void btReady_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btReady, "Ready Moments");
        }

        private void btPending_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btPending, "Pending Moments");
        }

        private void button7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(button7, "Search");
        }

        private void button5_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(button7, "Manage moments");
        }

        private void btManageMoments_Click(object sender, EventArgs e)
        {
            if (MomentsInitialization.listOfUsersSeries.Count != 0)
            {
                btManageMoments.Text = string.Empty;
                hidePanels(pnlManageMoments);
                pnlManageMoments.Location = new Point(270, pnlManageMoments.Location.Y);
                pnlManageMoments.Controls.Clear();
                //MomentsInitialization.buildListOfUsersSeries();

                try
                {
                    foreach (TV_Series show in MomentsInitialization.listOfUsersSeries)
                    {
                        pnlManageMoments.Controls.Add(new Content(show));
                    }
                }
                catch { }
            }
            else
            {
                MessageBox.Show("You are not following any of the featured content yet", "Content Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void hidePanels(Panel pnl)
        {
            foreach (Control p in panel4.Controls)
            {
                if (p is Panel)
                {
                    if (p.Name != "panel3")
                    {
                        if (p.Visible) { lastPanel = p.Name; } //Keep as last panel the only one which is currently visbile, before hidding it
                        p.Visible = false;
                    }
                }
            }
            pnl.Visible = true;
            pnl.Location = new Point(157, 62);
        }

        private void btReady_Click(object sender, EventArgs e)
        {
            if (DownloadManager._torrents.Count > 0)
            {
                hidePanels(pnlReady);
                pnlReady.BringToFront();
            }
            else
            {
                MessageBox.Show("No active download", "Downloader Feedback", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btPending_Click(object sender, EventArgs e)
        {
            int x = File.ReadAllLines(MomentsInitialization.path_readyMomentsFile).Count();
            if (x > 0)
            {
                hidePanels(pnlPending);
                loadPendingMoments();
            }
            else
            {
                MessageBox.Show("No episode downloaded yet!", "Ready to watch", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void Home_Move(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                //this.Hide();
                notifyIcon1.ShowBalloonTip(1000, "Moments is running", "Close", ToolTipIcon.Info);
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.ShowInTaskbar = true;
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowInTaskbar = true;
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //=================== PNLPENDING LOADING =====================///
        public void loadPendingMoments()
        {
            pnlPending.Controls.Clear();
            panelPendingActions.buildListOfreadyToWatchMoments();
            panelPendingActions.buildCategories();
            foreach (contentToWatchCategory cat in panelPendingActions.listOfCategories)
            {
                pnlPending.Controls.Add(cat);
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            hidePanels(pnlMesssage);
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            hidePanels(pnlHome);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            hidePanels(pnlMesssage);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string advertissment = string.Empty;

            //Verify if necessary fiels are not empty
            if (message_BodyTXT.Text == string.Empty)
            {
                advertissment += "Please type your message inside the body field!";
            }
            else if (message_BodyTXT.Text.Length <= 1)
            {
                advertissment += "Please type in a whole message" + Environment.NewLine;
            }

            if (message_ObjectTXT.Text == string.Empty)
            {
                advertissment += "Please add an object to your mesage which stands as a header or title" + Environment.NewLine;
            }
            else if (message_ObjectTXT.Text.Length <= 1)
            {
                advertissment += "Your message object is not valid" + Environment.NewLine;
            }

            if (message_TypeTXT.Text == string.Empty)
            {
                advertissment += "Please choose a type of issue that your message covers" + Environment.NewLine;
            }


            if (advertissment == string.Empty)
            {
                //send the essage
                try
                {
                    string qryInsert = "insert into Message values(default, @user_id, @msgType, @msgObject, @msgBody, default)";
                    MySqlConnection connectToUMessageTable = new MySqlConnection("Server=natinstance.caiwfdg20kux.us-west-1.rds.amazonaws.com;Database=natdb;UID=master_NAT;PWD=pass4NATInstance");
                    MySqlCommand cmdInsert = new MySqlCommand(qryInsert, connectToUMessageTable);

                    cmdInsert.Parameters.AddWithValue("@user_id", MomentsInitialization.user_id);
                    cmdInsert.Parameters.AddWithValue("@msgType", message_TypeTXT.Text);
                    cmdInsert.Parameters.AddWithValue("@msgObject", message_ObjectTXT.Text);
                    cmdInsert.Parameters.AddWithValue("@msgBody", message_BodyTXT.Text);

                    connectToUMessageTable.Open();
                    cmdInsert.ExecuteNonQuery();
                    connectToUMessageTable.Close();
                    message_BodyTXT.Text = string.Empty;
                    message_ObjectTXT.Text = string.Empty;
                    message_TypeTXT.Text = string.Empty;

                    MessageBox.Show("Message sent", "Message Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    hidePanels(pnlHome);
                }
                catch (Exception x)
                {
                    MessageBox.Show("Message could not be sent" + Environment.NewLine + "Details :" + x.Message, "Message Errors", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(advertissment, "Message Errors", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


        private void pnlHome_Paint(object sender, PaintEventArgs e)
        {
            pnlHome.Size = new Size(794, 293);
            pnlHome.Location = new Point(30, 62);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            lbCurrentlyDownloading.Visible = true;
            button9.BackColor =  Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(64)))));;
            button10.BackColor = Color.Gray;
            lblEmpty.Visible = true;
            lbRecentDownloads.Visible = false;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            lbRecentDownloads.Visible = true;
            button10.BackColor =  Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(69)))), ((int)(((byte)(64)))));;
            button9.BackColor = Color.Gray;
            lblEmpty.Visible = false;
            lbCurrentlyDownloading.Visible = false;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            System.Threading.Tasks.Task t = System.Threading.Tasks.Task.Run(() => MomentsInitialization.populateMomentsUsersSeries());
        }

        private void btUserAccount_Click(object sender, EventArgs e)
        {
            hidePanels(pnlUserAccount);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            pnlAccDetails.Visible = true;
            pnlAccInbox.Visible = false;
            pnlAccDetails.Location = new Point(159, 4);
            loadAccountSettings();

        }


        private void button12_Click(object sender, EventArgs e)
        {
            pnlAccDetails.Visible = false;
            pnlAccInbox.Visible = true;
            pnlAccInbox.Location = new Point(159, 4);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            switch (button16.Text)
            {
                //if edit, we enable all editable fields
                case "edit":

                    accDet_City.Enabled = true;
                    accDet_City.BackColor = Color.White;

                    accDet_MailAddress.Enabled = true;
                    accDet_MailAddress.BackColor = Color.White;

                    accDet_Name.Enabled = true;
                    accDet_Name.BackColor = Color.White;

                    accDet_PrefLanguage.Enabled = true;
                    accDet_PrefLanguage.BackColor = Color.White;

                    button16.Text = "cancel";
                    break;
                //if edit, we redisable all editable fields Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
                case "cancel":
                    loadAccountSettings();
                    accDet_City.Enabled = false;
                    accDet_City.BackColor = Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123))))); ;

                    accDet_MailAddress.Enabled = false;
                    accDet_MailAddress.BackColor = Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123))))); ;

                    accDet_Name.Enabled = false;
                    accDet_Name.BackColor = Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123))))); ;

                    accDet_PrefLanguage.Enabled = false;
                    accDet_PrefLanguage.BackColor = Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123))))); ;

                    button16.Text = "edit";
                    break;
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            MomentsInitialization.user_Account.editSetting("user_name", accDet_Name.Text);
            MomentsInitialization.user_Account.editSetting("user_mailaddress", accDet_MailAddress.Text);
            MomentsInitialization.user_Account.editSetting("user_city", accDet_City.Text);
            MomentsInitialization.user_Account.editSetting("user_preferredlanguage", accDet_PrefLanguage.Text);

            System.Threading.Tasks.Task t = System.Threading.Tasks.Task.Run(() =>
            {
                string qryUpdate = "update MomentUser set user_name=@user_name, user_email=@user_email, user_city=@user_city, user_language=@user_language where user_id=@user_id;";
                MySqlConnection conObj = MomentsInitialization.connectionToServer;

                MySqlCommand cmdDetails = new MySqlCommand(qryUpdate, conObj);

                cmdDetails.Parameters.AddWithValue("@user_name", MomentsInitialization.user_Account.userName);
                cmdDetails.Parameters.AddWithValue("@user_email", MomentsInitialization.user_Account.userMailAddress);
                cmdDetails.Parameters.AddWithValue("@user_city", MomentsInitialization.user_Account.userCity);
                cmdDetails.Parameters.AddWithValue("@user_language", MomentsInitialization.user_Account.userLanguage);
                cmdDetails.Parameters.AddWithValue("@user_id", MomentsInitialization.user_Account.acc_userID);

                conObj.Open();
                cmdDetails.ExecuteNonQuery();
                conObj.Close();
            });
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://tulz.co.za/publish.html");
        }

        private void label2_MouseClick(object sender, MouseEventArgs e)
        {
            System.Diagnostics.Process.Start("http://tulz.co.za/");
        }
        #endregion


        #region Window positioninng handler
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void Home_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        #endregion


        public static void loadAccountSettings()
        {
            MomentsInitialization.frmHome.accDet_City.Text = MomentsInitialization.user_Account.userCity;
            MomentsInitialization.frmHome.accDet_ID.Text = MomentsInitialization.user_Account.acc_userID;
            MomentsInitialization.frmHome.accDet_MailAddress.Text = MomentsInitialization.user_Account.userMailAddress;
            MomentsInitialization.frmHome.accDet_Name.Text = MomentsInitialization.user_Account.userName;
            MomentsInitialization.frmHome.accDet_PrefLanguage.Text = MomentsInitialization.user_Account.userLanguage;
        }
    }
}