﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using System.Net;
using System.Windows.Forms;
using System.IO;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Deployment.Application;

namespace Moments
{
    public class UpdateChecker
    {
        public static bool checkAgain = true;
        public static Thread thread_checkForUpdate = new Thread(new ThreadStart(() => 
            {
                thread_checkForUpdate.IsBackground = true;


                
                while (checkAgain)
                {//refreshoing the list of popular shows will change checkagain to true

                    #region check software update
                    try
                    {
                        thread_checkForUpdate.IsBackground = true;
                        WebRequest rq = WebRequest.Create("http://tulz.co.za/publish.html");

                        rq.Credentials = CredentialCache.DefaultCredentials;

                        HttpWebResponse response = (HttpWebResponse)rq.GetResponse();

                        Stream dataStream = response.GetResponseStream();

                        StreamReader reader = new StreamReader(dataStream);

                        string htmlCode = reader.ReadToEnd();

                        string versionFromPage = htmlCode.Substring(htmlCode.IndexOf("AppInfo"));
                        versionFromPage = versionFromPage.Substring(0, versionFromPage.LastIndexOf("AppInfo "));
                        versionFromPage = versionFromPage.Substring(versionFromPage.IndexOf("Version"));
                        versionFromPage = versionFromPage.Substring(0, versionFromPage.IndexOf("COLSPAN"));
                        versionFromPage = versionFromPage.Substring(versionFromPage.LastIndexOf("WIDTH"));
                        versionFromPage = versionFromPage.Substring(versionFromPage.IndexOf("<TD>"));
                        versionFromPage = versionFromPage.Substring(0, versionFromPage.IndexOf("</TD>"));
                        versionFromPage = versionFromPage.Substring(4);

                        if (versionFromPage != MomentsInitialization.appCurrrentVersion)
                        {
                            MomentsInitialization.frmHome.btUpdate.Invoke(new Action(() => MomentsInitialization.frmHome.btUpdate.Visible = true));

                            /*
                            DialogResult res = MessageBox.Show("The new version of the software is finally out! Do you wish to restart the applicatioin to update it?" + Environment.NewLine + "New Version: " + versionFromPage, "New Release Available", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (res == DialogResult.Yes)
                            {
                                
                                //Start a new instance of the current program
                                //System.Diagnostics.Process.Start(Application.ExecutablePath);

                                //close the current application process
                                //System.Diagnostics.Process.GetCurrentProcess().Kill();
                                //Application.Exit();

                                System.Diagnostics.Process.Start("Moments.application");
                                Application.Exit();
                            }
                            */
                            checkAgain = false;
                        }
                        else {
                            MomentsInitialization.frmHome.btUpdate.Invoke(new Action(() => MomentsInitialization.frmHome.btUpdate.Visible = false));
                            Thread.Sleep(60000);
                        }
                    }



                    catch { }
                    #endregion

                    //Thread.Sleep(6999);
                }
            }));
    }

    
}
