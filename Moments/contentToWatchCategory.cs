﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Moments
{
    public partial class contentToWatchCategory : UserControl
    {
        public int numberOfEpisodes { get; set; }
        public List<ReadyMoment> episodes = new List<ReadyMoment>();
        public contentToWatchCategory(string title)
        {
            InitializeComponent();
            this.Name = title;            
        }

        public void addEpisode(ReadyMoment rm) {
            episodes.Add(rm);
            numberOfEpisodes++;
        }

        private void btShow_Click(object sender, EventArgs e)
        {
            if (!pnlEpisodes.Visible)
            {
                pnlEpisodes.Visible = true;
            }
            else
            {
                pnlEpisodes.Visible = false;
            }
            
        }

        private void loadEpisodes() {
            foreach (ReadyMoment rm in episodes) {
                pnlEpisodes.Controls.Add(rm);
            }
        }

        private void contentToWatchCategory_Load(object sender, EventArgs e)
        {
            btShow.Text = this.Name + " [" + numberOfEpisodes + " episodes]";
            loadEpisodes();
        }
    }
}
