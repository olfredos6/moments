version: 2
#Designated Survivor
*Description: A low-level Cabinet member becomes President of the United States after a catastrophic attack kills everyone above him in the Presidential line of succession.
*Genre: Drama, Thriller
*Year: 2016
*Number of Episodes: 17
*Episodes already out: 15
*Air day: Wednesday
*Air time: 22:00:00
*Cast: Kiefer Sutherland, Maggie Q, Italia Ricci, Natascha McElhone, etc.
*Source: http://cyro.se/episodes/search.php?dayq=designated+survivor
*Image: http://cyro.se/uploads/thumbnails/Designated-Survivor-TMNS.png
#24 Legacy
*Description: A military hero who returns to the U.S. with a whole lot of trouble following him back. With nowhere else to turn, the man asks CTU to help him save his life while also stopping one of the largest-scale terror attacks on American soil.
*Genre: Action, Crime, Drama
*Year: 2017
*Number of Episodes: 24
*Episodes already out: 10
*Air day: Sunday
*Air time: 22:00:00
*Cast: Corey Hawkins, Charlie Hofheimer, Miranda Otto
*Source: http://cyro.se/episodes/search.php?dayq=24+legacy
*Image: http://cyro.se/uploads/thumbnails/24-Legacy-TMNS.png
#Pure Genius
*Description: A young Silicon Valley tech-titan enlists a veteran surgeon with a controversial past in starting a hospital with a cutting-edge, new school approach to medicine.
*Genre: Drama
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Thursday
*Air time: 22:00:00
*Cast: Kelly Albanese, Keon Alexander, Eva Ariel Binder 
*Source: http://cyro.se/episodes/search.php?dayq=pure+genius
*Image: http://cyro.se/uploads/thumbnails/Pure-Genius-TMNS.png
#Homeland
*Description: Taking place several months after the previous season, Carrie Mathison is back in the United States, living in Brooklyn, New York. She is now working at a foundation whose efforts are to provide aid to Muslims living in the United States. The season features a presidential election, and takes place between election day and inauguration day. The season will also discuss the Joint Comprehensive Plan of Action.
*Genre: Crime, Drama, Mystery 
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Sunday
*Air time: 21:00:00
*Cast: Claire Danes, Mandy Patinkin, Rupert Friend 
*Source: http://cyro.se/episodes/search.php?dayq=homeland
*Image: http://cyro.se/uploads/thumbnails/Homeland-TMNS.jpg
#The Blacklist
*Description: A new FBI profiler, Elizabeth Keen, has her entire life uprooted when a mysterious criminal, Raymond Reddington, who has eluded capture for decades, turns himself in and insists on speaking only to her.
*Genre: Crime, Drama, Mystery 
*Year: 2017
*Number of Episodes: 10
*Episodes already out: 0
*Air day: Thursday
*Air time: 22:00:00
*Cast: James Spader, Megan Boone, Diego Klattenhoff 
*Source: http://cyro.se/episodes/search.php?dayq=the+blacklist
*Image: http://cyro.se/uploads/thumbnails/The-Blacklist-TMNS.jpg
#Quantico
*Description: A look at the lives of young FBI recruits training at the Quantico base in Virginia when one of them is suspected of being a sleeper terrorist.
*Genre: Crime, Drama, Mystery 
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Sunday
*Air time: 22:00:00
*Cast: Priyanka Chopra, Jake McLaughlin, Aunjanue Ellis 
*Source: http://cyro.se/episodes/search.php?dayq=quantico
*Image: http://cyro.se/uploads/thumbnails/Quantico-TMNS.jpg
#Grey's Anatomy
*Description: A drama centered on the personal and professional lives of five surgical interns and their supervisors.
*Genre: Drama, Romance 
*Year: 2017
*Number of Episodes: 10
*Episodes already out: 10
*Air day: Thursday
*Air time: 22:00:00
*Cast: Ellen Pompeo, Justin Chambers, Chandra Wilson 
*Source: http://cyro.se/episodes/search.php?dayq=anatomy
*Image: http://cyro.se/uploads/thumbnails/Greys-Anatomy-S12-TMNS.jpg
#Blindspot
*Description: A mysterious tattooed woman has lost her memory and does not know her own identity. The FBI discovers that each tattoo contains a clue to a crime they will have to solve.
*Genre: Crime, Drama, Mystery 
*Year: 2017
*Number of Episodes: 10
*Episodes already out: 10
*Air day: Wednesday
*Air time: 22:00:00
*Cast: Sullivan Stapleton, Jaimie Alexander, Rob Brown 
*Source: http://cyro.se/episodes/search.php?dayq=blindspot
*Image: http://cyro.se/uploads/thumbnails/Blindspot-S02-TMNS.png
#The Flash
*Description: Barry Allen wakes up 9 months after he was struck by lightning and discovers that the bolt gave him the power of super speed. With his new team and powers, Barry becomes "The Flash" and fights crime in Central City.
*Genre: Action, Adventure, Drama 
*Year: 2017
*Number of Episodes: 10
*Episodes already out: 10
*Air day: Tuesday
*Air time: 20:00:00
*Cast: Greg Berlanti, Geoff Johns, Andrew Kreisberg 
*Source: http://cyro.se/episodes/search.php?dayq=the+flash
*Image: http://cyro.se/uploads/thumbnails/The-Flash-TMNS.jpg
#Supergirl
*Description: Kara Zor-El was sent to Earth from the doomed planet Krypton as a 12-year-old by her parents. Alura gave her instructions to protect her infant cousin Kal-El, and informed her that she, like her cousin, would have extraordinary powers under Earth's yellow sun
*Genre: Action, Adventure, Drama 
*Year: 2017
*Number of Episodes: 0 
*Episodes already out: 0
*Air day: Monday
*Air time: 20:00:00
*Cast: Melissa Benoist, Chyler Leigh, Mehcad Brooks 
*Source: http://cyro.se/episodes/search.php?dayq=supergirl
*Image: http://cyro.se/uploads/thumbnails/Supergirl-4DO.SE.jpg
#Empire
*Description: A hip-hop mogul must choose a successor among his three sons who are battling for control over his multi-million dollar company, while his ex-wife schemes to reclaim what is hers.
*Genre: Drama, Music 
*Year: 2016
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Wednesday
*Air time: 21:00:00
*Cast: Terrence Howard, Taraji P. Henson, Jussie Smollett
*Source: http://cyro.se/episodes/search.php?dayq=empire
*Image: http://cyro.se/uploads/thumbnails/Empire-TMNS.jpg
#Prison Break
*Description: Seven years later, thanks to information provided by T-Bag, Lincoln and Sara discover that Michael is still alive in a Yemen prison.
*Genre: Action, Crime, Drama 
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Tuesday
*Air time: 21:00:00
*Cast: Rockmond Dunbar, Robert Knepper, Wentworth Miller 
*Source: http://cyro.se/episodes/search.php?dayq=prison+break
*Image: http://cyro.se/uploads/thumbnails/Prison-Break-TMNS.png
#Scorpion
*Description: An eccentric genius forms an international network of super-geniuses to act as the last line of defense against the complicated threats of the modern world.
*Genre: Action, Drama 
*Year: 2016
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Monday
*Air time: 21:00:00
*Cast: Elyes Gabel, Katharine McPhee, Eddie Kaye Thomas 
*Source: http://cyro.se/episodes/search.php?dayq=scorpion
*Image: http://cyro.se/uploads/thumbnails/Scorpion-G2G.FM.jpg
#Supernatural
*Description: Two brothers follow their father's footsteps as "hunters" fighting evil supernatural beings of many kinds including monsters, demons, and gods that roam the earth.
*Genre: Drama, Fantasy, Horror
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Thursday
*Air time: 21:00:00
*Cast: Jared Padalecki, Jensen Ackles, Jim Beaver
*Source: http://cyro.se/episodes/search.php?dayq=supernatural
*Image: http://cyro.se/uploads/thumbnails/Supernatural-TMNS.jpg
#Humans
*Description: In a parallel present where the latest must-have gadget for any busy family is a 'Synth' - a highly-developed robotic servant that's so similar to a real human it's transforming the way we live.
*Genre: Drama, Sci-Fi 
*Year: 2015
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Sunday
*Air time: 17:00:00
*Cast: Gemma Chan, Katherine Parkinson, Lucy Carless
*Source: http://cyro.se/episodes/search.php?dayq=humans
*Image: http://cyro.se/uploads/thumbnails/Humans-4DO.SE.jpg
#Teen Wolf
*Description: A somewhat awkward teen is attacked by a werewolf and inherits the curse himself, as well as the enemies that come with it.
*Genre: Action, Comedy, Drama
*Year: 2011
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Tuesday
*Air time: 21:00:00
*Cast: Dylan O'Brien, Tyler Posey, Holland Roden
*Source: http://cyro.se/episodes/search.php?dayq=teen+wolf
*Image: http://cyro.se/uploads/thumbnails/Teen-Wolf-TMNS.png
#The Grand Tour
*Description: Follow Jeremy, Richard, and James, as they embark on an adventure across the globe. Driving new and exciting automobiles from manufactures all over the world.
*Genre: Comedy, Reality-TV 
*Year: 2016
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Thursday
*Air time: 22:00:00
*Cast: Jeremy Clarkson, Richard Hammond, James May
*Source: http://cyro.se/episodes/search.php?dayq=the+grand+tour
*Image: http://cyro.se/uploads/thumbnails/The-Grand-Tour-TMNS.png
#Shadowhunters
*Description: After her mother is kidnapped, Clary must venture into the dark world of demon hunting.
*Genre: Action, Drama, Fantasy 
*Year: 2016
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Monday
*Air time: 20:00:00
*Cast: Katherine McNamara, Dominic Sherwood, Matthew Daddario.
*Source: http://cyro.se/episodes/search.php?dayq=shadowhunters
*Image: http://cyro.se/uploads/thumbnails/Shadowhunters-TMNS.png
#Colony
*Description: In the not-too-distant future, Los Angeles has been invaded and occupied by outside forces, causing a rift between the city's residents; some have collaborated with the occupation, while others are rebelling and suffering the consequences that come with that choice.
*Genre: Action, Adventure, Sci-Fi 
*Year: 2016
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Thursday
*Air time: 22:00:00
*Cast: Josh Holloway, Sarah Wayne Callies, Isabella Crovetti 
*Source: http://cyro.se/episodes/search.php?dayq=colony
*Image: http://cyro.se/uploads/thumbnails/Colony-TMNS.png
#The Magicians
*Description: After being recruited to a secretive academy, a group of students discover that the magic they read about as children is very real-and more dangerous than they ever imagined.
*Genre: Drama, Fantasy, Horror 
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Wednesday
*Air time: 21:00:00
*Cast: Jason Ralph, Stella Maeve, Olivia Taylor Dudley 
*Source: http://cyro.se/watch/the-magicians/s2/e13
*Image: http://cyro.se/uploads/full/The-Magicians-TMNS.png
#iZombie
*Description: A medical resident finds that being a zombie has its perks, which she uses to assist the police.
*Genre: Comedy, Crime, Drama 
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Tuesday
*Air time: 21:00:00
*Cast: Rose McIver, Malcolm Goodwin, Rahul Kohli
*Source: http://cyro.se/watch/izombie/s3/e3
*Image: http://cyro.se/uploads/thumbnails/iZombie-TMNS.jpg
#Gotham
*Description: The story behind Detective James Gordon's rise to prominence in Gotham City in the years before Batman's arrival.
*Genre: Action, Crime, Drama 
*Year: 2016
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Monday
*Air time: 20:00:00
*Cast: Ben McKenzie, Jada Pinkett Smith, Donal Logue
*Source: http://cyro.se/episodes/search.php?dayq=gotham
*Image: http://cyro.se/uploads/thumbnails/Gotham-TMNS.jpg
#Into the Badlands
*Description: A mighty warrior and a young boy search for enlightenment in a ruthless territory controlled by feudal barons.
*Genre: Action, Adventure, Drama 
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Sunday
*Air time: 22:00:00
*Cast: Daniel Wu, Orla Brady, Sarah Bolger 
*Source: http://cyro.se/episodes/search.php?dayq=into+the+badlands
*Image: http://cyro.se/uploads/thumbnails/Into-the-Badlands-TMNS.jpg
#Taken
*Description: As former CIA agent Bryan Mills deals with a personal tragedy that shakes his world, he fights to overcome the incident and exact revenge.
*Genre: Action, Thriller 
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Monday
*Air time: 22:00:00
*Cast: As former CIA agent Bryan Mills deals with a personal tragedy that shakes his world, he fights to overcome the incident and exact revenge.
*Source: http://cyro.se/episodes/search.php?dayq=taken
*Image: http://cyro.se/uploads/thumbnails/Taken-TMNS.png
#Better Call Saul
*Description: The trials and tribulations of criminal lawyer, Jimmy McGill, in the time leading up to establishing his strip-mall law office in Albuquerque, New Mexico.
*Genre: Crime, Drama 
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Monday
*Air time: 22:00:00
*Cast: Michael McKean, Bob Odenkirk, Jonathan Banks 
*Source: http://cyro.se/episodes/search.php?dayq=better+call+saul
*Image: http://cyro.se/uploads/thumbnails/Better-Call-Saul-TMNS.png
#Guerrilla
*Description: A pair of activists in 1970s London set out to free a political prisoner and wage a resistance movement.
*Genre: Drama
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Sunday
*Air time: 23:00:00
*Cast: Idris Elba, Freida Pinto, Babou Ceesay 
*Source: http://cyro.se/episodes/search.php?dayq=guerrilla
*Image: http://cyro.se/uploads/thumbnails/Guerrilla-TMNS.png
#Marvel's Agents of S.H.I.E.L.D
*Description: The missions of the Strategic Homeland Intervention, Enforcement and Logistics Division.
*Genre: Action, Drama, Sci-Fi 
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Tuesday
*Air time: 22:00:00
*Cast: Clark Gregg, Ming-Na Wen, Brett Dalton
*Source: http://cyro.se/episodes/search.php?dayq=Agents+of+S.H.I.E.L.D.
*Image: http://cyro.se/uploads/thumbnails/Agents-of-S.H.I.E.L.D.-TMNS.jpg
#Arrow
*Description: Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow.
*Genre: Action, Adventure, Crime 
*Year: 2015
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Wednesday
*Air time: 20:00:00
*Cast: Stephen Amell, Katie Cassidy, David Ramsey
*Source: http://cyro.se/episodes/search.php?dayq=arrow
*Image: http://cyro.se/uploads/thumbnails/Arrow-TMNS.jpg
#The 100
*Description: Set 97 years after a nuclear war has destroyed civilization, when a spaceship housing humanity's lone survivors sends 100 juvenile delinquents back to Earth in hopes of possibly re-populating the planet.
*Genre: Drama, Mystery, Sci-Fi 
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Wednesday
*Air time: 21:00:00
*Cast: Eliza Taylor, Eli Goree, Thomas McDonell
*Source: http://cyro.se/episodes/search.php?dayq=the+100
*Image: http://cyro.se/uploads/thumbnails/The-100-TMNS.png
#The Big Bang Theory
*Description: A woman who moves into an apartment across the hall from two brilliant but socially awkward physicists shows them how little they know about life outside of the laboratory.
*Genre: Comedy
*Year: 2017
*Number of Episodes: 0
*Episodes already out: 0
*Air day: Monday
*Air time: 20:00:00
*Cast: Johnny Galecki, Jim Parsons, Kaley Cuoco
*Source: http://cyro.se/episodes/search.php?dayq=big+bang+theory
*Image: http://cyro.se/uploads/thumbnails/The-Big-Bang-Theory-TMNS.jpg