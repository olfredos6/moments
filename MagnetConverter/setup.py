#!/usr/bin/env python

from distutils.core import setup
import py2exe
from glob import glob
import sys

sys.path.append("C:\\Users\\Heavenly Demon\\Source\\Repos\\Moments\\MagnetConverter\\include\\")
data_files = [("DATA", glob(r'C:\Users\Heavenly Demon\Source\Repos\Moments\MagnetConverter\include\*.*'))]
setup(data_files=data_files)

setup(console=['converter.py'])
