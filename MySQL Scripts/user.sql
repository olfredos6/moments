use natdb;

drop table user;

create table user(
	email varchar(255) not null primary key,
    pass varchar(16) not null,
    accountIsVerified bool not null
);