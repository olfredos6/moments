use natdb;
drop table Message;

create table Message(
	Message_ID int auto_increment not null primary key,
    Message_UserID int not null,
    Message_Type varchar(100) not null,
    Message_Object text	null,
    Message_Body text not null
)